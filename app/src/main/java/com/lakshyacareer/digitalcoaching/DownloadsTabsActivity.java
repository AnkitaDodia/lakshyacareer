package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.adapter.SlidingPagerAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.ui.SlidingTabLayout;

public class DownloadsTabsActivity extends MasterActivity {

    Context mContext;
    TextView txt_action_title;
    LinearLayout layout_back;

    ViewPager vp_downloads;
    SlidingTabLayout stl_downloads;
    SlidingPagerAdapter adapter;

    String[] mStrings = {"Subjects", "Materials"};
    int Numboftabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads_tab);

        mContext = this;

        txt_action_title = (TextView) findViewById(R.id.txt_action_title);
        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        vp_downloads = findViewById(R.id.vp_downloads);
        stl_downloads = findViewById(R.id.stl_downloads);

        txt_action_title.setTypeface(getBoldTypeFace());
        txt_action_title.setText("My Download");

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new SlidingPagerAdapter(getSupportFragmentManager(), mStrings, Numboftabs);
        vp_downloads.setAdapter(adapter);

        stl_downloads.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        stl_downloads.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        stl_downloads.setViewPager(vp_downloads);

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fromMyDownloads = true;
        finish();
    }

}
