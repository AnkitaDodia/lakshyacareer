package com.lakshyacareer.digitalcoaching.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.lakshyacareer.digitalcoaching.model.Data;

import java.io.Serializable;

public class SettingsPreferences implements Serializable {

	private static final String PREFS_NAME = "LakshyaCareerDB";

	private static final String DEFAULT_VAL = null;

	private static final String Key_user_id = "id_user";
	private static final String Key_user_name = "first_name";
	private static final String Key_user_last_name= "last_name";
	private static final String Key_user_dob= "dob";
	private static final String Key_user_email = "email";
	private static final String Key_user_city = "city";
	private static final String Key_user_state = "state";
	private static final String Key_payment_mode = "payment_mode";
	private static final String Key_user_mobile_number = "mobile_number";
	private static final String Key_payment_status = "payment_status";

	public static void clearDB(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();
		dbEditor.clear();
		dbEditor.commit();

	}

	public static void storeConsumer(Context context, Data user) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();

		dbEditor.putString(Key_user_id, user.getIdUser());
		dbEditor.putString(Key_user_name, user.getFirstName());
		dbEditor.putString(Key_user_last_name, user.getLastName());
		dbEditor.putString(Key_user_dob, user.getDob());
		dbEditor.putString(Key_user_email, user.getEmail());
		dbEditor.putString(Key_user_city, user.getCity());
		dbEditor.putString(Key_user_state, user.getState());
		dbEditor.putString(Key_payment_mode, user.getPaymentMode());
		dbEditor.putString(Key_user_mobile_number, user.getMobileNumber());
		dbEditor.putString(Key_payment_status, user.getPaymentStatus());

		dbEditor.commit();
	}

	public static Data getConsumer(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);

		Data user = new Data();

		user.setCity(prefs.getString(Key_user_city, DEFAULT_VAL));
		user.setEmail(prefs.getString(Key_user_email, DEFAULT_VAL));
		user.setIdUser(prefs.getString(Key_user_id, DEFAULT_VAL));
		user.setFirstName(prefs.getString(Key_user_name, DEFAULT_VAL));
		user.setLastName(prefs.getString(Key_user_last_name, DEFAULT_VAL));
		user.setDob(prefs.getString(Key_user_dob, DEFAULT_VAL));
		user.setMobileNumber(prefs.getString(Key_user_mobile_number, DEFAULT_VAL));
		user.setPaymentMode(prefs.getString(Key_payment_mode, DEFAULT_VAL));
		user.setState(prefs.getString(Key_user_state, DEFAULT_VAL));
		user.setPaymentStatus(prefs.getString(Key_payment_status,DEFAULT_VAL));

		return user;
	}
}
