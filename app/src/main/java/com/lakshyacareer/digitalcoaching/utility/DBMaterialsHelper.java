package com.lakshyacareer.digitalcoaching.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lakshyacareer.digitalcoaching.model.MaterialsDownloads;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by My 7 on 11/11/2017.
 */
public class DBMaterialsHelper extends SQLiteOpenHelper
{
    private static String DB_PATH = "";
    private static final String DB_NAME = "LakshyCareer.sqlite";
    private SQLiteDatabase myDataBase;
    private final Context myContext;
    private static DBMaterialsHelper mDBConnection;
    static int version_val = 1;

    public DBMaterialsHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        this.myContext = context;
        DB_PATH = "/data/data/"
                + context.getApplicationContext().getPackageName()
                + "/databases/";
    }

    public static synchronized DBMaterialsHelper getDBAdapterInstance(Context context)
    {
        if (mDBConnection == null)
        {
            mDBConnection = new DBMaterialsHelper(context, DB_NAME, null,version_val);
        }
        return mDBConnection;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDataBase() throws IOException
    {
        boolean dbExist = checkDataBase();
        if (dbExist)
        {
            // do nothing - database already exist
        }
        else
        {
            this.getReadableDatabase();
            try
            {
                copyDataBase();
            }
            catch (IOException e)
            {
                throw new Error("Error copying database:" + e.toString());
            }
        }
    }

    public boolean databaseExist() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private boolean checkDataBase() {

        File dbFile = myContext.getDatabasePath(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    public long InsertMaterials(MaterialsDownloads model) {
        // TODO Auto-generated method stub
        long rawId = 0;
        try {
            ContentValues CV = new ContentValues();

            CV.put("material_id",model.getMaterialId());
            CV.put("material_name",model.getMaterialName());
            CV.put("material_category_name",model.getMaterialCategoryName());
            CV.put("material_path",model.getMaterialPath());
            CV.put("material_size",model.getMaterialSize());

            rawId = myDataBase.insert("materials", null, CV);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rawId;
    }

    public ArrayList<MaterialsDownloads> GetMaterials()
    {
        // TODO Auto-generated method stub
        ArrayList<MaterialsDownloads> list = new ArrayList<MaterialsDownloads>();
        Cursor cursor = myDataBase.rawQuery("SELECT * FROM materials", null);
        if (cursor.moveToFirst())
        {
            do
            {
                MaterialsDownloads bean = new MaterialsDownloads();
                bean.setMaterialId(cursor.getString(4));
                bean.setMaterialName(cursor.getString(0));
                bean.setMaterialCategoryName(cursor.getString(1));
                bean.setMaterialPath(cursor.getString(2));
                bean.setMaterialSize(cursor.getString(3));

                list.add(bean);

            } while (cursor.moveToNext());
        }
        Log.e("Size", "Size : "+list.size());
        cursor.close();
        return list;
    }

    public void deleteRow(String materialid) {
        myDataBase.delete("materials", "material_id=" + materialid, null);
    }
}
