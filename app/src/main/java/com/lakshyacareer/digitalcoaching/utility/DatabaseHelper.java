package com.lakshyacareer.digitalcoaching.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by My 7 on 11/11/2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    private static String DB_PATH = "";
    private static final String DB_NAME = "LakshyCareer.sqlite";
    private SQLiteDatabase myDataBase;
    private final Context myContext;
    private static DatabaseHelper mDBConnection;
    static int version_val = 1;

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context, name, factory, version);
        this.myContext = context;
        DB_PATH = "/data/data/"
                + context.getApplicationContext().getPackageName()
                + "/databases/";
    }

    public static synchronized DatabaseHelper getDBAdapterInstance(Context context)
    {
        if (mDBConnection == null)
        {
            mDBConnection = new DatabaseHelper(context, DB_NAME, null,version_val);
        }
        return mDBConnection;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDataBase() throws IOException
    {
        boolean dbExist = checkDataBase();
        if (dbExist)
        {
            // do nothing - database already exist
        }
        else
        {
            this.getReadableDatabase();
            try
            {
                copyDataBase();
            }
            catch (IOException e)
            {
                throw new Error("Error copying database:" + e.toString());
            }
        }
    }

    public boolean databaseExist() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private boolean checkDataBase() {

        File dbFile = myContext.getDatabasePath(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    public long Insert_Record(MyDownloadsList model) {
        // TODO Auto-generated method stub
        long rawId = 0;
        try {
            ContentValues CV = new ContentValues();

            CV.put("video_subject_name",model.getVideo_subject_name());
            CV.put("video_topic_name",model.getVideo_topic_name());
            CV.put("video_path",model.getVideo_path());
            CV.put("video_duration",model.getVideo_duration());
            CV.put("video_topic_download_id",model.getVideo_topic_download_id());

            rawId = myDataBase.insert("mydownlods", null, CV);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rawId;
    }

    public ArrayList<MyDownloadsList> Select_Record()
    {
        // TODO Auto-generated method stub
        ArrayList<MyDownloadsList> list = new ArrayList<MyDownloadsList>();
        Cursor cursor = myDataBase.rawQuery("SELECT * FROM mydownlods", null);
        if (cursor.moveToFirst())
        {
            do
            {
                MyDownloadsList bean = new MyDownloadsList();
                bean.setVideo_topic_download_id(cursor.getString(0));
                bean.setVideo_id(cursor.getString(1));
                bean.setVideo_subject_name(cursor.getString(2));
                bean.setVideo_topic_name(cursor.getString(3));
                bean.setVideo_path(cursor.getString(4));
                bean.setVideo_duration(cursor.getString(5));

                list.add(bean);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public void deleteRow(String meal_id) {
        myDataBase.delete("mydownlods", "video_id=" + meal_id, null);
    }
}
