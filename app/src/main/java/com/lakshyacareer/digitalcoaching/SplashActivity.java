package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;

public class SplashActivity extends MasterActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Context ctx;

    String TAG = "SplashActivity";
    TextView txt_developername, txt_developerby, txt_app_title, txt_way_to_tag;

    SharedPreferences prefs = null;
    ProgressBar mPrg_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ctx = this;
        getView();
        setTypeFace();

        prefs = getSharedPreferences("FIRSTRUN", MODE_PRIVATE);

        if (prefs.getBoolean("firstrun", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            prefs.edit().putBoolean("firstrun", false).commit();
            isFreshInstall = 1;

            Log.e(TAG, "FIRSTRUN  ?  Yes");
        } else {

            isFreshInstall = 0;
            Log.e(TAG, "FIRSTRUN  ?  No");
        }

        mPrg_splash.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (getLogin() == 1) {
                    Intent i = new Intent(ctx, NavigationDrawerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(ctx, SampleIntroActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void getView() {

        txt_developerby = (TextView) findViewById(R.id.txt_developerby);
        txt_developername = (TextView) findViewById(R.id.txt_developername);
        txt_way_to_tag = (TextView) findViewById(R.id.txt_way_to_tag);
        txt_app_title = (TextView) findViewById(R.id.txt_app_title);

        mPrg_splash = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void setTypeFace() {

        txt_app_title.setTypeface(getBoldTypeFace());
        txt_way_to_tag.setTypeface(getLightTypeFace());

        txt_developerby.setTypeface(getLightTypeFace());
        txt_developername.setTypeface(getBoldTypeFace());
    }
}
