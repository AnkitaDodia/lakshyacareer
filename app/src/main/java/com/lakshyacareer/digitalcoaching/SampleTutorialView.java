package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;

/**
 * Created by abc on 21-10-2017.
 */

public class SampleTutorialView extends MasterActivity {

    Context ctx;
    String TAG = "SampleTutorials";
    int stopPosition;
    VideoView vidView;

    private MediaController mediaControls;

    String vidAddress ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sampletutorialview);

        ctx = this;

        vidAddress = getIntent().getExtras().getString("video_path");

//        vidAddress = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
//        vidAddress = "http://admin.lakshyacareer.in/uploads/AuDagYWrSh.mp4";
        Log.e(TAG, "PATH  "+vidAddress);

       /* if(vidAddress.equals("null")){

            vidAddress = "android.resource://" + getPackageName() + "/" + R.raw.gujarati_vyakaran;
        }*/


        //set the media controller buttons
        if (mediaControls == null) {
            mediaControls = new MediaController(ctx);
        }


        vidView = (VideoView)findViewById(R.id.myVideo);

        /*String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
//        https://s3.envato.com/h264-video-previews/eb11c0bd-f864-487c-bd99-5fd8edcf8d79/15627045.mp4
        Uri vidUri = Uri.parse(vidAddress);

        MediaController vidControl = new MediaController(this);
        vidControl.setAnchorView(vidView);
        vidView.setMediaController(vidControl);

        vidView.setVideoURI(vidUri);
        vidView.start();*/

        showWaitIndicator(true);

        try {
            //set the media controller in the VideoView
            vidView.setMediaController(mediaControls);

            //set the uri of the video to be played
            vidView.setVideoURI(Uri.parse(vidAddress));
//            vidView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.gujarati_vyakaran));

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        vidView.requestFocus();
        //we also set an setOnPreparedListener in order to know when the video file is ready for playback
        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {
                // close the progress bar and play the video
                showWaitIndicator(false);
                //if we have a position on savedInstanceState, the video playback should start from here
                vidView.seekTo(stopPosition);
                if (stopPosition == 0) {
                    vidView.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
//                    vidView.pause();
                    vidView.start();
                }
            }
        });
    }

    /*@Override
    public void onPause() {
        Log.d(TAG, "onPause called");
        super.onPause();
        stopPosition = vidView.getCurrentPosition(); //stopPosition is an int
        vidView.pause();
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
        vidView.seekTo(stopPosition);
        vidView.start(); //Or use resume() if it doesn't work. I'm not sure
    }*/

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", vidView.getCurrentPosition());
        vidView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        stopPosition = savedInstanceState.getInt("Position");
        vidView.seekTo(stopPosition);
//        vidView.start();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}