package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.DeleteVideo;
import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 09-11-2017.
 */

public class MyDownloadsActivity extends MasterActivity {

    Context mContext;

    TextView txt_action_title,error_msg;

    LinearLayout layout_back,error_layout_topic;

    private boolean isInternet;

    ListView list_mydownloads;

    private ArrayList<MyDownloadsList> mMyDownloadlist = new ArrayList<>();

    String msg;

    private DatabaseHelper mDbHelper, dbAdapters;
    private SQLiteDatabase mDb;
    static int version_val = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_downloads);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        initView();
        setTypeFace();
        setClickListener();

        mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
        dbAdapters = DatabaseHelper.getDBAdapterInstance(mContext);
        try {
            dbAdapters.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setListData();
    }

    private void initView() {
        txt_action_title = (TextView) findViewById(R.id.txt_action_title);
        error_msg = (TextView) findViewById(R.id.error_msg);

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        error_layout_topic = (LinearLayout) findViewById(R.id.error_layout);

        list_mydownloads = (ListView) findViewById(R.id.list_mydownloads);

        txt_action_title.setText("My Download");
    }

    private void setTypeFace()
    {
        txt_action_title.setTypeface(getBoldTypeFace());
        error_msg.setTypeface(getLightTypeFace());
    }

    private void setClickListener()
    {
        fromMyDownloads = true;
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setListData()
    {
        try {
            dbAdapters.openDataBase();
            mMyDownloadlist = dbAdapters.Select_Record();
            dbAdapters.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("SIZE",""+mMyDownloadlist.size());

        if(mMyDownloadlist.size() > 0)
        {
            error_layout_topic.setVisibility(View.GONE);
            list_mydownloads.setVisibility(View.VISIBLE);


            DownloadListAdapter mDownloadListAdapter= new DownloadListAdapter(MyDownloadsActivity.this, mMyDownloadlist);
            list_mydownloads.setAdapter(mDownloadListAdapter);
        }
        else
        {
            error_layout_topic.setVisibility(View.VISIBLE);
            list_mydownloads.setVisibility(View.GONE);
            error_msg.setText("You haven't downloaded anything!!");
        }
    }

    public class DownloadListAdapter extends BaseAdapter
    {
        ArrayList<MyDownloadsList> mMyDownloadlist = new ArrayList<>();

        MyDownloadsActivity mContext;

        MyDownloadHolder holder;

        public DownloadListAdapter(MyDownloadsActivity context, ArrayList<MyDownloadsList> MyDownloadlist)
        {
            mContext = context;
            mMyDownloadlist = MyDownloadlist;
        }

        @Override
        public int getCount() {
            return mMyDownloadlist.size();
        }

        @Override
        public Object getItem(int position) {
            return mMyDownloadlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            holder = null;
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.row_my_downloads, null);
                holder = new MyDownloadHolder();

                holder.txt_number = (TextView) convertView.findViewById(R.id.txt_number_download);
                holder.txt_subject_name = (TextView) convertView.findViewById(R.id.txt_subject_name_download);
                holder.txt_topic_name = (TextView) convertView.findViewById(R.id.txt_topic_name_download);
                holder.txt_duration_download = (TextView) convertView.findViewById(R.id.txt_duration_download);

                holder.img_delete = (ImageView) convertView.findViewById(R.id.img_delete_downloads);
//            holder.img_favourite = (ImageView) convertView.findViewById(R.id.img_favourite);
                holder.img_play = (ImageView) convertView.findViewById(R.id.img_play_downloads);

                holder.lay_play_downloads = (LinearLayout) convertView.findViewById(R.id.lay_play_downloads);
                holder.lay_delete_downloads = (LinearLayout) convertView.findViewById(R.id.lay_delete_downloads);

                convertView.setTag(holder);
            } else {

                holder = (MyDownloadHolder) convertView.getTag();
            }

            final MyDownloadsList mMyDownloaditems = mMyDownloadlist.get(i);

            holder.txt_number.setTypeface(mContext.getBoldTypeFace());
            holder.txt_subject_name.setTypeface(mContext.getBoldTypeFace());
            holder.txt_topic_name.setTypeface(mContext.getRegularTypeFace());
            holder.txt_duration_download.setTypeface(mContext.getRegularTypeFace());

            int pos = i + 1;
            holder.txt_number.setText(String.valueOf(pos));
            holder.txt_subject_name.setText(mMyDownloaditems.getVideo_topic_name());
            holder.txt_topic_name.setText(mMyDownloaditems.getVideo_subject_name());
            holder.txt_duration_download.setText("Duration : "+mMyDownloaditems.getVideo_duration());

            holder.lay_delete_downloads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dbAdapters.openDataBase();
                        dbAdapters.deleteRow(mMyDownloadlist.get(i).getVideo_id());
                        dbAdapters.close();

                        File fdelete = new File(mMyDownloadlist.get(i).getVideo_path());
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                System.out.println("file Deleted :" + mMyDownloadlist.get(i).getVideo_path());
                            } else {
                                System.out.println("file not Deleted :" + mMyDownloadlist.get(i).getVideo_path());
                            }
                        }
                        System.out.println("file not Deleted :" + mMyDownloadlist.get(i).getVideo_topic_download_id());
                        sendVideoDeleteRequest(mMyDownloadlist.get(i).getVideo_topic_download_id());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.lay_play_downloads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent videoIntent = new Intent(mContext, VideoPlayerActivity.class);
                    videoIntent.putExtra("video_path",mMyDownloaditems.getVideo_path());
                    mContext.startActivity(videoIntent);
                }
            });

            return convertView;
        }

        class MyDownloadHolder {

            TextView txt_number, txt_subject_name, txt_topic_name,txt_duration_download;
            ImageView img_favourite, img_delete, img_play;
            LinearLayout lay_play_downloads, lay_delete_downloads;
        }

        private void sendVideoDeleteRequest(String video_topic_download_id)
        {
            mContext.showWaitIndicator(true);

            Log.e("Download test", ""+video_topic_download_id);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendDeleteDownloadedRequest(video_topic_download_id,new Callback<DeleteVideo>() {
                @Override
                public void success(DeleteVideo content, Response response) {

                    mContext.showWaitIndicator(false);

                    Log.e("DELETE_VIDEO_RESPONSE", "" + new Gson().toJson(content));

                    if (response.getStatus() == 200) {
                        try {
                            if(content.getStatus() == 1)
                            {
                                notifyDataSetChanged();

                                setListData();
                            }
                            else
                            {
                                Log.e("ERROR",content.getMessage());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fromMyDownloads = true;
        finish();
    }
}
