package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.adapter.FAQListAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.FAQData;
import com.lakshyacareer.digitalcoaching.model.FAQitems;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 31-10-2017.
 */

public class FAQActivity extends MasterActivity {

    Context mContext;
    private boolean isInternet;

    ListView list_faq;

    ArrayList<FAQitems> mFAQList = new ArrayList<>();

    private FAQListAdapter mFAQListAdapter;

    TextView txt_action_title;

    LinearLayout layout_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        initView();
        setTypeFace();
        setClickListener();

        if(MasterActivity.mFAQitems.size() > 0)
        {
            mFAQList = mFAQitems ;
            setListData();
        }
        else
        {
            if (isInternet) {
                getFaqListRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView() {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        list_faq = (ListView) findViewById(R.id.list_faq);

        txt_action_title = (TextView)findViewById(R.id.txt_action_title);

        txt_action_title.setText("FAQs");
    }

    private void setTypeFace() {
        txt_action_title.setTypeface(getBoldTypeFace());
    }

    private void setClickListener() {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getFaqListRequest() {
        try {

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getFAQListRequest(new Callback<FAQData>() {
                @Override
                public void success(FAQData model, Response response) {

                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SUBJECT_LIST_RESPONSE", "" + new Gson().toJson(model));

                            mFAQList = model.getFaqData();

                            mFAQitems = mFAQList ;

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {


                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
//                    Toast.makeText(mContext,error.getResponse().getReason(), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setListData() {

        mFAQListAdapter = new FAQListAdapter(FAQActivity.this, mFAQList);

        // setting list adapter
        list_faq.setAdapter(mFAQListAdapter);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}