package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;

/**
 * Created by abc on 01-11-2017.
 */

public class LegalActivity extends MasterActivity {

    Context mContext;

    TextView txt_action_title,txt_privacy_policy,txt_return_policy,txt_terms_and_conditions,txt_intellectual_property,txt_disclaimer,
             txt_process_flow;

    LinearLayout layout_back, lay_privacy_policy, lay_return_policy, lay_terms_and_conditions, lay_intlectual_property, lay_disclaimer, lay_process_flow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_legal);

        mContext = this;

        initView();
        setTypeFace();
        setClickListener();

        txt_action_title.setText("Legal");
    }

    private void initView() {
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_privacy_policy = (TextView)findViewById(R.id.txt_privacy_policy);
        txt_return_policy = (TextView)findViewById(R.id.txt_return_policy);
        txt_terms_and_conditions = (TextView)findViewById(R.id.txt_terms_and_conditions);
        txt_intellectual_property = (TextView)findViewById(R.id.txt_intellectual_property);
        txt_disclaimer = (TextView)findViewById(R.id.txt_disclaimer);
        txt_process_flow = (TextView)findViewById(R.id.txt_process_flow);

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        lay_privacy_policy = (LinearLayout) findViewById(R.id.lay_privacy_policy);
        lay_return_policy = (LinearLayout) findViewById(R.id.lay_return_policy);
        lay_terms_and_conditions = (LinearLayout) findViewById(R.id.lay_terms_and_conditions);
        lay_intlectual_property = (LinearLayout) findViewById(R.id.lay_intlectual_property);
        lay_disclaimer = (LinearLayout) findViewById(R.id.lay_disclaimer);
        lay_process_flow = (LinearLayout) findViewById(R.id.lay_process_flow);
    }

    private void setTypeFace() {
        txt_action_title.setTypeface(getBoldTypeFace());
        txt_privacy_policy.setTypeface(getRegularTypeFace());
        txt_return_policy.setTypeface(getRegularTypeFace());
        txt_terms_and_conditions.setTypeface(getRegularTypeFace());
        txt_intellectual_property.setTypeface(getRegularTypeFace());
        txt_disclaimer.setTypeface(getRegularTypeFace());
        txt_process_flow.setTypeface(getRegularTypeFace());
    }

    private void setClickListener() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        lay_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, PrivacyPolicyActivity.class));
            }
        });

        lay_return_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, ReturnPolicyActivity.class));
            }
        });

        lay_terms_and_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, TermsConditionsActivity.class));
            }
        });

        lay_intlectual_property.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, IntellectualPropertyActivity.class));
            }
        });

        lay_disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, DisclaimerActivity.class));
            }
        });

        lay_process_flow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, ProcessFlowActivity.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}