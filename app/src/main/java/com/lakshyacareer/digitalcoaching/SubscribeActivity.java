package com.lakshyacareer.digitalcoaching;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Data;
import com.lakshyacareer.digitalcoaching.model.User;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 23-10-2017.
 */

public class SubscribeActivity extends MasterActivity {

    Context ctx;

    LinearLayout layout_back;
    TextView txt_action_title;

    TextInputLayout input_layout_first_name_reg, input_layout_last_name_reg, input_layout_dob_reg, input_layout_email_reg, input_layout_mobile_reg,
            input_layout_password_reg, input_layout_confirm_pass_reg,input_layout_city_reg; //input_layout_state_reg

    EditText edit_first_name_reg, edit_last_name_reg, edit_dob_reg, edit_email_reg, edit_mobile_reg, edit_pass_reg, edit_confirm_pass_reg,
             edit_city_reg; //edit_state_reg,

    Button btn_subscribe;
    private boolean isInternet;

    String IMEI_number, StateName;

    Spinner spinner_state;

    int selectedItem = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();
        setSpinnerAdapter();

        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
//            IMEI_number = Static_IMEI;
            Log.e("IMEI_NUMBER" , IMEI_number);
        }
    }

    private void getView() {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_action_title = (TextView) findViewById(R.id.txt_action_title);

        btn_subscribe = (Button) findViewById(R.id.btn_subscribe);

        spinner_state = (Spinner) findViewById(R.id.spinner_state);

        input_layout_first_name_reg = (TextInputLayout) findViewById(R.id.input_layout_first_name_reg);
        input_layout_last_name_reg = (TextInputLayout) findViewById(R.id.input_layout_last_name_reg);
        input_layout_email_reg = (TextInputLayout) findViewById(R.id.input_layout_email_reg);
        input_layout_mobile_reg = (TextInputLayout) findViewById(R.id.input_layout_mobile_reg);
        input_layout_password_reg = (TextInputLayout) findViewById(R.id.input_layout_password_reg);
        input_layout_confirm_pass_reg = (TextInputLayout) findViewById(R.id.input_layout_confirm_pass_reg);
//        input_layout_state_reg = (TextInputLayout) findViewById(R.id.input_layout_state_reg);
        input_layout_city_reg = (TextInputLayout) findViewById(R.id.input_layout_city_reg);
        input_layout_dob_reg = (TextInputLayout) findViewById(R.id.input_layout_dob_reg);

        edit_first_name_reg = (EditText) findViewById(R.id.edit_first_name_reg);
        edit_last_name_reg = (EditText) findViewById(R.id.edit_last_name_reg);
        edit_email_reg = (EditText) findViewById(R.id.edit_email_reg);
        edit_mobile_reg = (EditText) findViewById(R.id.edit_mobile_reg);
        edit_pass_reg = (EditText) findViewById(R.id.edit_pass_reg);
        edit_confirm_pass_reg = (EditText) findViewById(R.id.edit_confirm_pass_reg);
//        edit_state_reg = (EditText) findViewById(R.id.edit_state_reg);
        edit_city_reg = (EditText) findViewById(R.id.edit_city_reg);
        edit_dob_reg = (EditText) findViewById(R.id.edit_dob_reg);

        edit_dob_reg.addTextChangedListener(mTextWatcher);

        txt_action_title.setText("Subscribe");

//        SaveRegistrationData();
    }

    private void setTypeFace() {
        txt_action_title.setTypeface(getBoldTypeFace());

        btn_subscribe.setTypeface(getBoldTypeFace());

        input_layout_first_name_reg.setTypeface(getRegularTypeFace());
        input_layout_last_name_reg.setTypeface(getRegularTypeFace());
        input_layout_email_reg.setTypeface(getRegularTypeFace());
        input_layout_mobile_reg.setTypeface(getRegularTypeFace());
        input_layout_password_reg.setTypeface(getRegularTypeFace());
        input_layout_confirm_pass_reg.setTypeface(getRegularTypeFace());
//        input_layout_state_reg.setTypeface(getRegularTypeFace());
        input_layout_city_reg.setTypeface(getRegularTypeFace());
        input_layout_dob_reg.setTypeface(getRegularTypeFace());

        edit_first_name_reg.setTypeface(getRegularTypeFace());
        edit_last_name_reg.setTypeface(getRegularTypeFace());
        edit_email_reg.setTypeface(getRegularTypeFace());
        edit_mobile_reg.setTypeface(getRegularTypeFace());
        edit_pass_reg.setTypeface(getRegularTypeFace());
        edit_confirm_pass_reg.setTypeface(getRegularTypeFace());
//        edit_state_reg.setTypeface(getRegularTypeFace());
        edit_city_reg.setTypeface(getRegularTypeFace());
        edit_dob_reg.setTypeface(getRegularTypeFace());
    }

    private void setClickListener() {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                StateName = adapterView.getItemAtPosition(i).toString();
                selectedItem = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String password = edit_pass_reg.getText().toString();
                String confirmPass = edit_confirm_pass_reg.getText().toString();

                if (edit_first_name_reg.getText().toString().length() == 0) {
                    edit_first_name_reg.setError("Please enter first name");
                } else if (edit_last_name_reg.getText().toString().length() == 0) {
                    edit_last_name_reg.setError("Please enter last name");
                } else if (edit_dob_reg.getText().toString().length() == 0) {
                    edit_dob_reg.setError("Please enter Date of Birth");
                } else if (edit_email_reg.getText().toString().length() == 0) {
                    edit_email_reg.setError("Please enter email");
                } else if (!isValidEmail(edit_email_reg.getText().toString())) {
                    edit_email_reg.setError("Please enter valid email");
                } else if (edit_mobile_reg.getText().toString().length() == 0) {
                    edit_mobile_reg.setError("Please enter mobile number");
                } else if (edit_mobile_reg.getText().toString().length() < 10) {
                    edit_mobile_reg.setError("Please enter valid mobile number");
                } else if (!isAlphaNumeric(password)) {
                    edit_pass_reg.setError("Please enter at alphanumeric password");
                } else if (password.length() == 0) {
                    edit_pass_reg.setError("Please enter valid password");
                } else if (password.length() < 6) {
                    edit_pass_reg.setError("Please enter at least six character password");
                } else if (confirmPass.length() == 0) {
                    edit_confirm_pass_reg.setError("Your password doesn't match");
                } else if (!confirmPass.equalsIgnoreCase(password)) {
                    edit_confirm_pass_reg.setError("Your password dose't match ");
                }
//                else if (edit_state_reg.getText().toString().length() == 0) {
//                    edit_state_reg.setError("Please define your state");
//                }
                else if (edit_city_reg.getText().toString().length() == 0) {
                    edit_city_reg.setError("Please define your city");
                } else {
                    if (isInternet) {
                        sendSubScripRequest();
//                        SaveRegistrationData();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

//                startActivity(new Intent(ctx, MerchantActivity.class));
            }
        });
    }

    private void setSpinnerAdapter()
    {
        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, states);



        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, states) {

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = null;
                v = super.getDropDownView(position, null, parent);
                // If this is the selected item position
                if (position == selectedItem) {
                    v.setBackgroundColor(Color.LTGRAY);
                }
                else {
                    // for other views
                    v.setBackgroundColor(Color.WHITE);

                }
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // attaching data adapter to spinner
        spinner_state.setAdapter(dataAdapter);
        spinner_state.setSelection(5,true);
    }

    TextWatcher mTextWatcher = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!s.toString().equals(current)) {
                String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
                String cleanC = current.replaceAll("[^\\d.]|\\.", "");

                int cl = clean.length();
                int sel = cl;
                for (int i = 2; i <= cl && i < 6; i += 2) {
                    sel++;
                }
                //Fix for pressing delete next to a forward slash
                if (clean.equals(cleanC)) sel--;

                if (clean.length() < 8) {
                    clean = clean + ddmmyyyy.substring(clean.length());
                } else {
                    //This part makes sure that when we finish entering numbers
                    //the date is correct, fixing it otherwise
                    int day = Integer.parseInt(clean.substring(0, 2));
                    int mon = Integer.parseInt(clean.substring(2, 4));
                    int year = Integer.parseInt(clean.substring(4, 8));

                    mon = mon < 1 ? 1 : mon > 12 ? 12 : mon;
                    cal.set(Calendar.MONTH, mon - 1);
                    year = (year < 1900) ? 1900 : (year > 2100) ? 2100 : year;
                    cal.set(Calendar.YEAR, year);
                    // ^ first set year for the line below to work correctly
                    //with leap years - otherwise, date e.g. 29/02/2012
                    //would be automatically corrected to 28/02/2012

                    day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                    clean = String.format("%02d%02d%02d", day, mon, year);
                }

                clean = String.format("%s-%s-%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8));

                sel = sel < 0 ? 0 : sel;
                current = clean;
                edit_dob_reg.setText(current);
                edit_dob_reg.setSelection(sel < current.length() ? sel : current.length());
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    /*private void SaveRegistrationData(){

        mFirstName = edit_first_name_reg.getText().toString();
        mLastName = edit_last_name_reg.getText().toString();
        mDOB = edit_dob_reg.getText().toString();
        mCity = edit_city_reg.getText().toString();
        mState = spinner_state.getSelectedItem().toString();
        mEmail = edit_email_reg.getText().toString();
        mPassword = edit_pass_reg.getText().toString();
        mConfirmPass = edit_confirm_pass_reg.getText().toString();
        mMobile = edit_mobile_reg.getText().toString();
        mPaymentMode = "1";
        IMEI_Number = IMEI_number;
        startActivity(new Intent(ctx, MerchantActivity.class));
        finish();

    }*/

    private void SaveRegistrationData(){

        edit_first_name_reg.setText("Hardik");
        edit_last_name_reg.setText("Kubavat");
        edit_city_reg.setText("Surat");
        edit_email_reg.setText("abc@xyz.com");
        edit_pass_reg.setText("kukdoo1");
        edit_confirm_pass_reg.setText("kukdoo1");
        edit_mobile_reg.setText("9662472875");
        mPaymentMode = "1";

    }

    private void sendSubScripRequest() {
        try {
            String fName = edit_first_name_reg.getText().toString();
            String lName = edit_last_name_reg.getText().toString();
            String dob = edit_dob_reg.getText().toString();
            String city = edit_city_reg.getText().toString();
            StateName = spinner_state.getSelectedItem().toString();
            String email = edit_email_reg.getText().toString();
            String password = edit_pass_reg.getText().toString();
            String confirmPass = edit_confirm_pass_reg.getText().toString();
            String mobile = edit_mobile_reg.getText().toString();

//            Log.e("hardz.kubavat@gmail.com",spinnerValue);

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendRegisterRequest(fName, lName, dob, email, mobile, password, confirmPass, StateName, city, IMEI_number, new Callback<User>() {
                        @Override
                        public void success(User model, Response response) {

                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("REGISTER_RESPONSE", "" + new Gson().toJson(model));

                                    if (model.getStatus() == 1) {
                                        Data data = model.getData();
                                        updateUser(data);

//                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();

                                        setRegister(true);

                                        startActivity(new Intent(ctx, PaymentOptionActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();
                                    }


                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showWaitIndicator(false);

//                            Toast.makeText(ctx, "failure", Toast.LENGTH_LONG).show();

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    IMEI_number = getIMEINumber();
//                    IMEI_number = Static_IMEI;
                    Log.e("IMEI_NUMBER_OVERRIDE" , IMEI_number);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}