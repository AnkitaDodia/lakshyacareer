package com.lakshyacareer.digitalcoaching.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.MaterialCategoriesActivity;
import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.MaterialsData;

import java.util.List;

/**
 * Created by abc on 25-10-2017.
 */

public class MaterialsListAdapter extends BaseAdapter {

    NavigationDrawerActivity mContext;

    private int selectFilter = 0;

    MaterialsListHolder holder;

    private List<MaterialsData> MaterialList;

    public MaterialsListAdapter(NavigationDrawerActivity context, List<MaterialsData> mMaterialList) {

        mContext = context;
        this.MaterialList = mMaterialList;
    }

    @Override
    public int getCount() {
        return MaterialList.size();
    }

    @Override
    public Object getItem(int position) {
        return MaterialList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_material, null);
            holder = new MaterialsListHolder();


            holder.txt_number_material = (TextView) convertView.findViewById(R.id.txt_number_material);
            holder.txt_title_materials = (TextView) convertView.findViewById(R.id.txt_title_materials);

            holder.card_materials = (CardView) convertView.findViewById(R.id.card_materials);

            convertView.setTag(holder);

        } else {

            holder = (MaterialsListHolder) convertView.getTag();
        }

        final MaterialsData mMaterials = MaterialList.get(position);

        holder.txt_title_materials.setTypeface(mContext.getBoldTypeFace());
        holder.txt_number_material.setTypeface(mContext.getBoldTypeFace());

        int pos = position + 1;
        holder.txt_number_material.setText(String.valueOf(pos));
        holder.txt_title_materials.setText(mMaterials.getCategoryName());

        holder.card_materials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MasterActivity.CategoryList.clear();
                MasterActivity.CategoryName = mMaterials.getCategoryName();
                MasterActivity.CategoryId = mMaterials.getIdCategory();
                Log.e("CategoryId",MasterActivity.CategoryId);
                mContext.startActivity(new Intent(mContext, MaterialCategoriesActivity.class));
            }
        });

      /*  if(mSelectionsList.get(position) == 1){
//            holder.filterName.setTextColor(mContext.getResources().getColor(R.color.theme_color));
            holder.img_container.setBackgroundResource(R.color.clg_palettes_soft_light);
        }else{
//            holder.filterName.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.img_container.setBackgroundResource(R.color.clg_palettes_hard_light);
        }*/

        return convertView;
    }


    class MaterialsListHolder {

        TextView txt_number_material, txt_title_materials ;
        CardView card_materials;
    }
}
