package com.lakshyacareer.digitalcoaching.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.LoginActivity;
import com.lakshyacareer.digitalcoaching.MaterialCategoriesActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.TopicListActivity;
import com.lakshyacareer.digitalcoaching.TopicViewActivity;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.CategoryData;
import com.lakshyacareer.digitalcoaching.model.TopicListData;

import java.util.ArrayList;

/**
 * Created by My 7 on 10/31/2017.
 */
public class CategoryAdapter extends BaseAdapter
{
    ArrayList<CategoryData> mCategoryList = new ArrayList<>();

    MaterialCategoriesActivity mContext;

    TopicListHolder holder;

    public CategoryAdapter(MaterialCategoriesActivity context, ArrayList<CategoryData> CategoryList)
    {
        mContext = context;
        mCategoryList = CategoryList;
    }

    @Override
    public int getCount() {
        return mCategoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_categoty, null);
            holder = new TopicListHolder();

            holder.txt_number_material = (TextView) convertView.findViewById(R.id.txt_number_material);
            holder.txt_title_category = (TextView) convertView.findViewById(R.id.txt_title_category);
            holder.img_pdf_download = (ImageView) convertView.findViewById(R.id.img_pdf_download);

            holder.ll_pdf_download = (LinearLayout) convertView.findViewById(R.id.ll_pdf_download);

            holder.card_category = (CardView) convertView.findViewById(R.id.card_category);

            convertView.setTag(holder);
        } else {

            holder = (TopicListHolder) convertView.getTag();
        }

        final CategoryData mCategoryData = mCategoryList.get(i);

        holder.txt_title_category.setTypeface(mContext.getBoldTypeFace());
        holder.txt_number_material.setTypeface(mContext.getBoldTypeFace());

        int pos = i + 1;
        holder.txt_number_material.setText(String.valueOf(pos));

        holder.txt_title_category.setText(mCategoryData.getTitle());

        holder.ll_pdf_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                MasterActivity.topicId = mCategoryData.getIdTopic();
//                if(mContext.getLogin() == 1)
//                {
//                    Intent it = new Intent(mContext, TopicViewActivity.class);
//                    mContext.startActivity(it);
//                }
//                else
//                {
//                    openDialog(mContext);
//                }
            }
        });

        return convertView;
    }

    class TopicListHolder {

        TextView txt_number_material, txt_title_category;
        LinearLayout ll_pdf_download;
        CardView card_category;
        ImageView img_pdf_download;
    }

    public void openDialog(final TopicListActivity mContext) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle("Lakshya Career");
        alertDialogBuilder.setMessage("You have not subscribed or logged in yet. Please subscribe or login to continue..!!").setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mContext.isFromTopicListing = 1;
                        mContext.startActivity(new Intent(mContext, LoginActivity.class));
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
        }).show();
    }
}
