package com.lakshyacareer.digitalcoaching.adapter;

import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.FAQActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.model.FAQitems;

import java.util.ArrayList;

/**
 * Created by abc on 31-10-2017.
 */

public class FAQListAdapter extends BaseAdapter {

    FAQActivity mContext;

    FAQListAdapter.FAQListHolder holder;

    private ArrayList<FAQitems> FAQList = new ArrayList<>();

    public FAQListAdapter(FAQActivity context, ArrayList<FAQitems>  mFAQList) {

        mContext = context;
        this.FAQList = mFAQList;
    }

    @Override
    public int getCount() {
        return FAQList.size();
    }

    @Override
    public Object getItem(int position) {
        return FAQList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_faq_list, null);
            holder = new FAQListAdapter.FAQListHolder();

            holder.txt_faq_questions = (TextView) convertView.findViewById(R.id.txt_faq_questions);
            holder.txt_faq_answer = (TextView) convertView.findViewById(R.id.txt_faq_answer);

            holder.card_faq = (CardView) convertView.findViewById(R.id.card_faq);

            convertView.setTag(holder);

        } else {

            holder = (FAQListAdapter.FAQListHolder) convertView.getTag();
        }

        final FAQitems mFAQitems= FAQList.get(position);

        holder.txt_faq_questions.setTypeface(mContext.getRegularTypeFace());
        holder.txt_faq_answer.setTypeface(mContext.getRegularTypeFace());

        holder.txt_faq_questions.setText(mFAQitems.getQuestion());
        holder.txt_faq_answer.setText(mFAQitems.getAnswer());


        /*holder.card_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/


        return convertView;
    }


    class FAQListHolder {

        TextView txt_faq_questions, txt_faq_answer;
        CardView card_faq;
    }
}