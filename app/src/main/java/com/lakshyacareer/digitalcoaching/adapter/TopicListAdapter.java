package com.lakshyacareer.digitalcoaching.adapter;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.LoginActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.TopicListActivity;
import com.lakshyacareer.digitalcoaching.TopicViewActivity;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.TopicListData;

import java.util.ArrayList;

/**
 * Created by My 7 on 10/31/2017.
 */
public class TopicListAdapter extends BaseAdapter
{
    ArrayList<TopicListData> mTopicList = new ArrayList<>();

    TopicListActivity mContext;

    TopicListHolder holder;

    String topicId;

    public TopicListAdapter(TopicListActivity context, ArrayList<TopicListData> topicList)
    {
        mContext = context;
        mTopicList = topicList;
    }

    @Override
    public int getCount() {
        return mTopicList.size();
    }

    @Override
    public Object getItem(int position) {
        return mTopicList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_topic_list, null);
            holder = new TopicListHolder();

            holder.textView_position = (TextView) convertView.findViewById(R.id.textView_position);
            holder.txt_topic_name = (TextView) convertView.findViewById(R.id.txt_topic_name);
            holder.txt_duration = (TextView) convertView.findViewById(R.id.txt_duration);
            holder.txt_size = (TextView) convertView.findViewById(R.id.txt_size);

            holder.btn_new_badge_topic = (Button) convertView.findViewById(R.id.btn_new_badge_topic);

            holder.card_view_topic = (CardView) convertView.findViewById(R.id.card_view_topic);

//            holder.img_save = (ImageView) convertView.findViewById(R.id.img_save);
//            holder.img_favorite = (ImageView) convertView.findViewById(R.id.img_favorite);
//            holder.img_play = (ImageView) convertView.findViewById(R.id.img_play);

            convertView.setTag(holder);
        } else {

            holder = (TopicListHolder) convertView.getTag();
        }

        final TopicListData mTopic = mTopicList.get(i);

        if(mTopic.getIsNew().equalsIgnoreCase("1")){
            holder.btn_new_badge_topic.setVisibility(View.VISIBLE);
        }else{
            holder.btn_new_badge_topic.setVisibility(View.INVISIBLE);
        }

        holder.textView_position.setTypeface(mContext.getBoldTypeFace());
        holder.txt_topic_name.setTypeface(mContext.getBoldTypeFace());
        holder.txt_duration.setTypeface(mContext.getRegularTypeFace());
        holder.txt_size.setTypeface(mContext.getRegularTypeFace());

        int pos = i + 1;
        holder.textView_position.setText(String.valueOf(pos));
        holder.txt_topic_name.setText(mTopic.getTopicName());
        holder.txt_duration.setText("Duration - "+mTopic.getDuration());
        holder.txt_size.setText("Size - "+mTopic.getSize());

        holder.card_view_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MasterActivity.topicId = mTopic.getIdTopic();
                if(mContext.getLogin() == 1)
                {
                    Intent it = new Intent(mContext, TopicViewActivity.class);
                    mContext.startActivity(it);
                }
                else
                {
                    openDialog(mContext);
                }
            }
        });

        return convertView;
    }

    class TopicListHolder {

        TextView textView_position, txt_topic_name, txt_duration,txt_size;
        Button btn_new_badge_topic;
        CardView card_view_topic;
    }

    public void openDialog(final TopicListActivity mContext) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle("Lakshya Career");
        alertDialogBuilder.setMessage("You have not subscribed or logged in yet. Please subscribe or login to continue..!!").setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mContext.isFromTopicListing = 1;
                        mContext.startActivity(new Intent(mContext, LoginActivity.class));
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
        }).show();
    }
}
