package com.lakshyacareer.digitalcoaching.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.TopicListActivity;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.SubjectlistData;

import java.util.List;

/**
 * Created by abc on 25-10-2017.
 */

public class SubjectListAdapter extends BaseAdapter {

    NavigationDrawerActivity mContext;

    private int selectFilter = 0;

    SubjectListHolder holder;

    private List<SubjectlistData> SubjectsList;

    public SubjectListAdapter(NavigationDrawerActivity context, List<SubjectlistData> mSubjectsList) {

        mContext = context;
        this.SubjectsList = mSubjectsList;
    }

    @Override
    public int getCount() {
        return SubjectsList.size();
    }

    @Override
    public Object getItem(int position) {
        return SubjectsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.row_subject_list, null);
            holder = new SubjectListHolder();

            holder.txt_tutor_name = (TextView) convertView.findViewById(R.id.txt_tutor_name);
            holder.txt_subject_title = (TextView) convertView.findViewById(R.id.txt_subject_title);
            holder.txt_subject_numb = (TextView) convertView.findViewById(R.id.txt_subject_numb);

            holder.btn_new_badge = (Button) convertView.findViewById(R.id.btn_new_badge);

            holder.card_view_subject = (CardView) convertView.findViewById(R.id.card_view_subject);

            convertView.setTag(holder);

        } else {

            holder = (SubjectListHolder) convertView.getTag();
        }

        final SubjectlistData mSubjects = SubjectsList.get(position);

        if(mSubjects.getIsNew().equalsIgnoreCase("1")){
            holder.btn_new_badge.setVisibility(View.VISIBLE);
        }else{
            holder.btn_new_badge.setVisibility(View.INVISIBLE);
        }

        holder.txt_subject_numb.setTypeface(mContext.getBoldTypeFace());
        holder.txt_subject_title.setTypeface(mContext.getBoldTypeFace());
        holder.txt_tutor_name.setTypeface(mContext.getRegularTypeFace());

        int pos = position + 1;
        holder.txt_subject_numb.setText(String.valueOf(pos));
        holder.txt_subject_title.setText(mSubjects.getSubjectName().trim());
        holder.txt_tutor_name.setText(mSubjects.getSubjectDetail().trim());


        holder.card_view_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MasterActivity.topicList.clear();
                MasterActivity.subjectName = mSubjects.getSubjectName();
                MasterActivity.subjectId = mSubjects.getIdSubject();
                Log.e("MasterActivity.subjectId",MasterActivity.subjectId);
                mContext.startActivity(new Intent(mContext, TopicListActivity.class));
            }
        });

      /*  if(mSelectionsList.get(position) == 1){
//            holder.filterName.setTextColor(mContext.getResources().getColor(R.color.theme_color));
            holder.img_container.setBackgroundResource(R.color.clg_palettes_soft_light);
        }else{
//            holder.filterName.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.img_container.setBackgroundResource(R.color.clg_palettes_hard_light);
        }*/

        return convertView;
    }


    class SubjectListHolder {

        TextView txt_subject_numb, txt_subject_title, txt_tutor_name;
        Button btn_new_badge;
        CardView card_view_subject;
    }
}
