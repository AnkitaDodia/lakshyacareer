package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Sample;
import com.lakshyacareer.digitalcoaching.model.SampleData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 21-10-2017.
 */

public class SampleIntroActivity extends MasterActivity {

    Context mContext;
    private boolean isInternet;

    private LinearLayout dotsLayout;
    private TextView[] dots;
    private Button btnSkip, btnNext;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;

    private int no_of_sample_tutorial = 3;


    ArrayList<Sample> mSampleList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_sampleintro);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView();
        setTypeFace();
        setClickListener();

//        setData();

        if (isInternet) {
            getSampleTutorialsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }


    }

    private void inItView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
    }

    private void setTypeFace()
    {
        btnSkip.setTypeface(getRegularTypeFace());
        btnNext.setTypeface(getRegularTypeFace());

        // adding bottom dots
        addBottomDots(0);
    }


    private void setClickListener()
    {
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < no_of_sample_tutorial) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {

                    startActivity(new Intent(mContext, DashBoardActivity.class));
                    finish();
                }
            }
        });
    }

    private void setData()
    {
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == no_of_sample_tutorial - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[no_of_sample_tutorial];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_dark));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light));
    }

    private void launchHomeScreen() {
        startActivity(new Intent(mContext, DashBoardActivity.class));
        finish();
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.sample_intro_slide, container, false);

            //Update Views API DATA Here

            TextView txt_sampletopic_name = (TextView)view.findViewById(R.id.txt_sampletopic_name);
            TextView txt_sample_tag = (TextView)view.findViewById(R.id.txt_sample_tag);

            ImageView img_slider = (ImageView)view.findViewById(R.id.img_slider);

            txt_sampletopic_name.setTypeface(getBoldTypeFace());
            txt_sample_tag.setTypeface(getRegularTypeFace());

            txt_sampletopic_name.setTextColor(getResources().getColor(R.color.colorAccent));
            txt_sample_tag.setTextColor(getResources().getColor(R.color.colorPrimaryDark));


            final Sample mSample= mSampleList.get(position);

            txt_sampletopic_name.setText(mSample.getTitle());
            txt_sample_tag.setText(mSample.getDescription());

            Glide.with(SampleIntroActivity.this).load(mSample.getImageUrl())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_slider);


            img_slider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent videointent = new Intent(mContext, VideoPlayerActivity.class);
                    videointent.putExtra("video_path",mSample.getUrl());
                    startActivity(videointent);
                }
            });


            /*((TextView)view.findViewById(R.id.txt_sampletopic_name)).setTextColor(getResources().getColor(R.color.colorAccent));
            ((TextView)view.findViewById(R.id.txt_sample_tag)).setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            ((ImageView)view.findViewById(R.id.img_slider)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startActivity(new Intent(ctx, SampleTutorialView.class));

                }
            });*/
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return no_of_sample_tutorial;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void getSampleTutorialsRequest() {
        try {

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getSampleTutorials(new Callback<SampleData>() {
                @Override
                public void success(SampleData model, Response response) {

                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SUBJECT_LIST_RESPONSE", "" + new Gson().toJson(model));

                            mSampleList = model.getSampleListData();

                            MasterActivity.mSampleList = mSampleList ;

                            setData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {


                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
//                    Toast.makeText(mContext,error.getResponse().getReason(), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
