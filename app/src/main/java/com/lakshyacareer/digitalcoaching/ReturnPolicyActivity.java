package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;

import java.io.InputStream;

/**
 * Created by abc on 03-11-2017.
 */

public class ReturnPolicyActivity extends MasterActivity {

    Context ctx;

    TextView txt_action_title, txt_content;
    LinearLayout layout_back;

    WebView webview_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_content);

        ctx = this;

        initViews();
        setClickListener();

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.return_policy);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_content.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();

        }

    }

    private void initViews(){

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_action_title.setText("Return Policy");
        txt_content = (TextView)findViewById(R.id.txt_content);

        webview_content = (WebView) findViewById(R.id.webview_content);
    }

    private void setClickListener(){

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}