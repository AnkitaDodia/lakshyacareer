//package com.lakshyacareer.digitalcoaching.service;
//
//import android.app.AlarmManager;
//import android.app.IntentService;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.SystemClock;
//import android.support.annotation.Nullable;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.lakshyacareer.digitalcoaching.MyDownloadsActivity;
//import com.lakshyacareer.digitalcoaching.R;
//import com.lakshyacareer.digitalcoaching.TopicViewActivity;
//import com.lakshyacareer.digitalcoaching.receivers.TopicViewReceiver;
//import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
//import com.thin.downloadmanager.DefaultRetryPolicy;
//import com.thin.downloadmanager.DownloadManager;
//import com.thin.downloadmanager.DownloadRequest;
//import com.thin.downloadmanager.DownloadStatusListenerV1;
//import com.thin.downloadmanager.RetryPolicy;
//import com.thin.downloadmanager.ThinDownloadManager;
//
//import java.io.File;
//
///**
// * Created by My 7 on 15-Dec-17.
// */
//
//public class DownloadService extends IntentService {
//    /**
//     * Creates an IntentService.  Invoked by your subclass's constructor.
//     *
//     * @param name Used to name the worker thread, important only for debugging.
//     */
//    private ThinDownloadManager downloadManager;
//    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
//
//    MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();
//
//    DownloadRequest downloadRequest;
//    int downloadId;
//
//    public String APP_PATH_SD_CARD = "/Subjects/";
//
//    private NotificationManager mNotifyManager;
//    private NotificationCompat.Builder build;
//    int id = 1;
//
//    String getTopicName,getIdTopic,getVideo,getUrl;
//    Uri destinationUri;
//
//    private boolean isInternet;
//
//    public DownloadService() {
//        super("DownloadService");
//
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//
//        Bundle bundle = intent.getExtras().getBundle("BUNDLE");
//        getTopicName = bundle.getString("NAME");
//        getIdTopic = bundle.getString("ID");
//        getVideo = bundle.getString("VIDEO");
//        getUrl = bundle.getString("URL");
//
//        Log.e("getTopicName",""+getTopicName);
//        Log.e("getIdTopic",""+getIdTopic);
//        Log.e("getVideo",""+getVideo);
//        Log.e("getUrl",""+getUrl);
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        isInternet = new InternetStatus().isInternetOn(this);
//
//        if (isInternet) {
//
//            initForDownload(getUrl);
//
//            if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
//                downloadId = downloadManager.add(downloadRequest);
//            }
//
//            build.setContentText("Download Started");
//            // Removes the progress bar
//            build.setProgress(0, 0, false);
//            mNotifyManager.notify(id, build.build());
//
//        } else {
//            Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public String CreateFileName(String filename){
//        return filename.replace(" ", "");
//    }
//
//    private void initForDownload(String DownloadURL)
//    {
//        APP_PATH_SD_CARD = APP_PATH_SD_CARD+"/Topics"+"/" ;
//
//        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
//        RetryPolicy retryPolicy = new DefaultRetryPolicy();
//
////        File filesDir = getExternalFilesDir("");
//        File filesDir = getExternalFilesDir(APP_PATH_SD_CARD);
//
//        String DestinationPath = filesDir+"/"+CreateFileName(getTopicName+"_"+getIdTopic+getVideo);
//
//
//        Log.e("DestinationPath","DestinationPath    :"+DestinationPath);
//
//
//        Uri downloadUri = Uri.parse(DownloadURL);
//        destinationUri = Uri.parse(DestinationPath);
//        downloadRequest = new DownloadRequest(downloadUri)
//                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.LOW)
//                .setRetryPolicy(retryPolicy)
//                .setDownloadContext("Download")
//                .setStatusListener(myDownloadStatusListener);
//
//
//        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        build = new NotificationCompat.Builder(this);
//        build.setContentTitle(""+getTopicName)
//                .setContentText("Download in progress")
//                .setSmallIcon(R.drawable.ic_file_download_white_18dp);
//
//        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
//                new Intent(this, MyDownloadsActivity.class),
//                0);
//
//        build.setContentIntent(resultPendingIntent);
//        build.setAutoCancel(true);
//    }
//
//    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {
//
//        @Override
//        public void onDownloadComplete(DownloadRequest request) {
//            final int id = request.getDownloadId();
//            if (id == downloadId) {
//                TopicViewActivity.mProgressTxt.setText(request.getDownloadContext() + "Download Completed");
//
//                build.setContentText("Download Completed");
//                // Removes the progress bar
//                build.setProgress(0, 0, false);
//                mNotifyManager.notify(id, build.build());
//
//                //Call Broadcast receiver
////                registerReceiver(new Intent(DownloadService.this, TopicViewReceiver.class));
//                Toast.makeText(DownloadService.this,"From service Call",Toast.LENGTH_LONG).show();
//                Intent it = new Intent(DownloadService.this,TopicViewReceiver.class);
//                sendBroadcast(it);
////                PendingIntent pendingIntent = PendingIntent.getBroadcast(DownloadService.this, 0, it, 0);
////                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
////                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 0, pendingIntent);
//            }
//        }
//
//        @Override
//        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
//            final int id = request.getDownloadId();
//            if (id == downloadId) {
//                TopicViewActivity.mProgressTxt.setText("Download Failed");
//                TopicViewActivity.mProgress.setProgress(0);
//
//                build.setContentText("Download Failed");
//                mNotifyManager.notify(id, build.build());
//            }
//        }
//
//        @Override
//        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
//            int id = request.getDownloadId();
//
//            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
//            if (id == downloadId) {
//                TopicViewActivity.mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
//                TopicViewActivity.mProgress.setProgress(progress);
//
//                build.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
//                build.setProgress(100, progress, false);
//                mNotifyManager.notify(id, build.build());
//
//            }
//        }
//    }
//
//    private String getBytesDownloaded(int progress, long totalBytes) {
//        //Greater than 1 MB
//        long bytesCompleted = (progress * totalBytes)/100;
//        if (totalBytes >= 1000000) {
//            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
//        } if (totalBytes >= 1000) {
//            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");
//
//        } else {
//            return ( ""+bytesCompleted+"/"+totalBytes );
//        }
//    }
//}
