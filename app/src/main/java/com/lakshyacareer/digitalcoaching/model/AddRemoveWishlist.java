package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/8/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddRemoveWishlist {

    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_topic")
    @Expose
    private String idTopic;
    @SerializedName("id_subject")
    @Expose
    private String idSubject;
    @SerializedName("id_topic_wishlist")
    @Expose
    private Integer idTopicWishlist;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(String idSubject) {
        this.idSubject = idSubject;
    }

    public Integer getIdTopicWishlist() {
        return idTopicWishlist;
    }

    public void setIdTopicWishlist(Integer idTopicWishlist) {
        this.idTopicWishlist = idTopicWishlist;
    }

}