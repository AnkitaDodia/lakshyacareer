package com.lakshyacareer.digitalcoaching.model;

import java.io.Serializable;

/**
 * Created by My 7 on 11/11/2017.
 */
public class MaterialsDownloads implements Serializable
{
    String MaterialId;
    String MaterialName;
    String MaterialCategoryName;
    String MaterialPath;
    String MaterialSize;

    public String getMaterialId() {
        return MaterialId;
    }

    public void setMaterialId(String materialId) {
        MaterialId = materialId;
    }

    public String getMaterialName() {
        return MaterialName;
    }

    public void setMaterialName(String materialName) {
        MaterialName = materialName;
    }

    public String getMaterialCategoryName() {
        return MaterialCategoryName;
    }

    public void setMaterialCategoryName(String materialCategoryName) {
        MaterialCategoryName = materialCategoryName;
    }

    public String getMaterialPath() {
        return MaterialPath;
    }

    public void setMaterialPath(String materialPath) {
        MaterialPath = materialPath;
    }

    public String getMaterialSize() {
        return MaterialSize;
    }

    public void setMaterialSize(String materialSize) {
        MaterialSize = materialSize;
    }

}
