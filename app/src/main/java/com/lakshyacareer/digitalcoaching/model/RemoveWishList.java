package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/8/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemoveWishList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("add_remove_wishlist")
    @Expose
    private String addRemoveWishlist;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddRemoveWishlist() {
        return addRemoveWishlist;
    }

    public void setAddRemoveWishlist(String addRemoveWishlist) {
        this.addRemoveWishlist = addRemoveWishlist;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}