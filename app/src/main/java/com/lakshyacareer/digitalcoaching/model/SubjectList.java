package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 10/30/2017.
 */
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubjectList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("subjectlist_data")
    @Expose
    private ArrayList<SubjectlistData> subjectlistData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<SubjectlistData> getSubjectlistData() {
        return subjectlistData;
    }

    public void setSubjectlistData(ArrayList<SubjectlistData> subjectlistData) {
        this.subjectlistData = subjectlistData;
    }

}