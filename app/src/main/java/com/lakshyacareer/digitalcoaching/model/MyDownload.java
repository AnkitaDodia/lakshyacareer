package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by abc on 09-11-2017.
 */

public class MyDownload {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("my_download_data")
    @Expose
    private ArrayList<MyDownloaditems> myDownloadData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MyDownloaditems> getMyDownloadData() {
        return myDownloadData;
    }

    public void setMyDownloadData(ArrayList<MyDownloaditems> myDownloadData) {
        this.myDownloadData = myDownloadData;
    }
}
