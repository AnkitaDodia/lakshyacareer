package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/8/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicViewData {

    @SerializedName("id_topic")
    @Expose
    private String idTopic;
    @SerializedName("id_subject")
    @Expose
    private String idSubject;
    @SerializedName("topic_name")
    @Expose
    private String topicName;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("is_new")
    @Expose
    private String isNew;
    @SerializedName("fevorite")
    @Expose
    private String fevorite;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("activated")
    @Expose
    private String activated;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("id_topic_wishlist")
    @Expose
    private Integer idTopicWishlist;
    @SerializedName("id_my_download")
    @Expose
    private Integer idMyDownload;
    @SerializedName("is_downloaded")
    @Expose
    private Integer isDownloaded;

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(String idSubject) {
        this.idSubject = idSubject;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getFevorite() {
        return fevorite;
    }

    public void setFevorite(String fevorite) {
        this.fevorite = fevorite;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Integer getIdTopicWishlist() {
        return idTopicWishlist;
    }

    public void setIdTopicWishlist(Integer idTopicWishlist) {
        this.idTopicWishlist = idTopicWishlist;
    }

    public Integer getIdMyDownload() {
        return idMyDownload;
    }

    public void setIdMyDownload(Integer idMyDownload) {
        this.idMyDownload = idMyDownload;
    }

    public Integer getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(Integer isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

}