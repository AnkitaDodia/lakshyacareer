package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/8/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicPlay {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("topic_play_data")
    @Expose
    private Integer topicPlayData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTopicPlayData() {
        return topicPlayData;
    }

    public void setTopicPlayData(Integer topicPlayData) {
        this.topicPlayData = topicPlayData;
    }
}