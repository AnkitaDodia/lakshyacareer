package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/7/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("add_remove_wishlist")
    @Expose
    private AddRemoveWishlist addRemoveWishlist;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddRemoveWishlist getAddRemoveWishlist() {
        return addRemoveWishlist;
    }

    public void setAddRemoveWishlist(AddRemoveWishlist addRemoveWishlist) {
        this.addRemoveWishlist = addRemoveWishlist;
    }
}