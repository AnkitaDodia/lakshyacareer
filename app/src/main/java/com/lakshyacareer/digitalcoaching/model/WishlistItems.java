package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 08-11-2017.
 */

public class WishlistItems {

    @SerializedName("id_topic_wishlist")
    @Expose
    private String idTopicWishlist;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_subject")
    @Expose
    private String idSubject;
    @SerializedName("id_topic")
    @Expose
    private String idTopic;
    @SerializedName("fevorite")
    @Expose
    private String fevorite;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("topic_name")
    @Expose
    private String topicName;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("is_downloaded")
    @Expose
    private String isDownloaded;

    public String getIdTopicWishlist() {
        return idTopicWishlist;
    }

    public void setIdTopicWishlist(String idTopicWishlist) {
        this.idTopicWishlist = idTopicWishlist;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(String idSubject) {
        this.idSubject = idSubject;
    }

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getFevorite() {
        return fevorite;
    }

    public void setFevorite(String fevorite) {
        this.fevorite = fevorite;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(String isDownloaded) {
        this.isDownloaded = isDownloaded;
    }
}
