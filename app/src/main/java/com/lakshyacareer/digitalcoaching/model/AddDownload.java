package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 09-11-2017.
 */

public class AddDownload {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("id_my_download")
    @Expose
    private Integer idMyDownload;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getIdMyDownload() {
        return idMyDownload;
    }

    public void setIdMyDownload(Integer idMyDownload) {
        this.idMyDownload = idMyDownload;
    }
}
