package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by abc on 08-11-2017.
 */

public class WishlistData {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("my_wishlist_data")
    @Expose
    private ArrayList<WishlistItems> myWishlistData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<WishlistItems> getMyWishlistData() {
        return myWishlistData;
    }

    public void setMyWishlistData(ArrayList<WishlistItems> myWishlistData) {
        this.myWishlistData = myWishlistData;
    }

}
