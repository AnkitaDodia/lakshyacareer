package com.lakshyacareer.digitalcoaching.model;

import java.io.Serializable;

/**
 * Created by My 7 on 11/11/2017.
 */
public class MyDownloadsList implements Serializable
{
    String video_id;
    String video_subject_name;
    String video_topic_name;
    String video_path;
    String video_duration;
    String video_topic_download_id;

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_subject_name() {
        return video_subject_name;
    }

    public void setVideo_subject_name(String video_subject_name) {
        this.video_subject_name = video_subject_name;
    }

    public String getVideo_topic_name() {
        return video_topic_name;
    }

    public void setVideo_topic_name(String video_topic_name) {
        this.video_topic_name = video_topic_name;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getVideo_duration() {
        return video_duration;
    }

    public void  setVideo_duration(String video_duration) {
        this.video_duration = video_duration;
    }

    public String getVideo_topic_download_id() {
        return video_topic_download_id;
    }

    public void setVideo_topic_download_id(String video_topic_download_id) {
        this.video_topic_download_id = video_topic_download_id;
    }
}
