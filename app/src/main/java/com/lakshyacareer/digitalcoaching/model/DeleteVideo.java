package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/16/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteVideo {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("add_remove_downloads")
    @Expose
    private String addRemoveDownloads;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAddRemoveDownloads() {
        return addRemoveDownloads;
    }

    public void setAddRemoveDownloads(String addRemoveDownloads) {
        this.addRemoveDownloads = addRemoveDownloads;
    }

}