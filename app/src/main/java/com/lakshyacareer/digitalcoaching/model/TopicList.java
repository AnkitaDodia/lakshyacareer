package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/7/2017.
 */
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("topic_list_data")
    @Expose
    private ArrayList<TopicListData> topicListData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TopicListData> getTopicListData() {
        return topicListData;
    }

    public void setTopicListData(ArrayList<TopicListData> topicListData) {
        this.topicListData = topicListData;
    }

}