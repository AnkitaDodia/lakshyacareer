package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 21-11-2017.
 */

public class MySubscription {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("payment_data")
    @Expose
    private MySubscriptionData paymentData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MySubscriptionData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(MySubscriptionData paymentData) {
        this.paymentData = paymentData;
    }

}