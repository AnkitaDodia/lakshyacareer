package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 09-11-2017.
 */

public class MyDownloaditems {

    @SerializedName("id_my_download")
    @Expose
    private String idMyDownload;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_subject")
    @Expose
    private String idSubject;
    @SerializedName("id_topic")
    @Expose
    private String idTopic;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("topic_name")
    @Expose
    private String topicName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;

    public String getIdMyDownload() {
        return idMyDownload;
    }

    public void setIdMyDownload(String idMyDownload) {
        this.idMyDownload = idMyDownload;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdSubject() {
        return idSubject;
    }

    public void setIdSubject(String idSubject) {
        this.idSubject = idSubject;
    }

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
