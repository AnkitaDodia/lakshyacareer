package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by My 7 on 11/8/2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicView {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("fevorite")
    @Expose
    private Integer fevorite;
    @SerializedName("topic_view_data")
    @Expose
    private TopicViewData topicViewData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFevorite() {
        return fevorite;
    }

    public void setFevorite(Integer fevorite) {
        this.fevorite = fevorite;
    }

    public TopicViewData getTopicViewData() {
        return topicViewData;
    }

    public void setTopicViewData(TopicViewData topicViewData) {
        this.topicViewData = topicViewData;
    }

}