package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abc on 21-11-2017.
 */

public class MySubscriptionData {

    @SerializedName("payed_amount")
    @Expose
    private String payedAmount;
    @SerializedName("payment_date")
    @Expose
    private String paymentDate;
    @SerializedName("next_payment_date")
    @Expose
    private String nextPaymentDate;
    @SerializedName("expired")
    @Expose
    private String expired;
    @SerializedName("today_date")
    @Expose
    private String todayDate;

    public String getPayedAmount() {
        return payedAmount;
    }

    public void setPayedAmount(String payedAmount) {
        this.payedAmount = payedAmount;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getNextPaymentDate() {
        return nextPaymentDate;
    }

    public void setNextPaymentDate(String nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public String getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(String todayDate) {
        this.todayDate = todayDate;
    }

}