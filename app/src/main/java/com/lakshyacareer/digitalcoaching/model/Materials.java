package com.lakshyacareer.digitalcoaching.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Materials {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private ArrayList<MaterialsData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<MaterialsData> getData() {
        return data;
    }

    public void setData(ArrayList<MaterialsData> data) {
        this.data = data;
    }
}
