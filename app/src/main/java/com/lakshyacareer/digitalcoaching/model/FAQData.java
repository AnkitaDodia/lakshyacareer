package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by abc on 31-10-2017.
 */

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FAQData {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("faq_data")
    @Expose
    private ArrayList<FAQitems> faqData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<FAQitems> getFaqData() {
        return faqData;
    }

    public void setFaqData(ArrayList<FAQitems> faqData) {
        this.faqData = faqData;
    }
}
