package com.lakshyacareer.digitalcoaching.model;

/**
 * Created by Adite-Ankita on 19-Aug-16.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content {

    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("terms")
    @Expose
    private String terms;

    /**
     *
     * @return
     * The about
     */
    public String getAbout() {
        return about;
    }

    /**
     *
     * @param about
     * The about
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     *
     * @return
     * The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     * The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     *
     * @return
     * The terms
     */
    public String getTerms() {
        return terms;
    }

    /**
     *
     * @param terms
     * The terms
     */
    public void setTerms(String terms) {
        this.terms = terms;
    }
}