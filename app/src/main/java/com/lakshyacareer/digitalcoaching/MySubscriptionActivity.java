package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.MySubscription;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 10-11-2017.
 */

public class MySubscriptionActivity extends MasterActivity {

    Context ctx;

    LinearLayout layout_back;
    TextView txt_action_title, txt_payed_amount, txt_payed_date, txt_nextpayment_date;


    private boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_subscription);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();

        if (isInternet){
            sendMySubscriptionRequest();
        }else {
            Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

    }

    private void getView(){

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_action_title.setText("My Subscription");

        txt_payed_amount = (TextView)findViewById(R.id.txt_payed_amount);
        txt_payed_date = (TextView)findViewById(R.id.txt_payed_date);
        txt_nextpayment_date = (TextView)findViewById(R.id.txt_nextpayment_date);
    }


    private void setTypeFace()
    {

        txt_action_title.setTypeface(getBoldTypeFace());

        txt_payed_amount.setTypeface(getBoldTypeFace());
        txt_payed_date.setTypeface(getBoldTypeFace());
        txt_nextpayment_date.setTypeface(getBoldTypeFace());
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    private void sendMySubscriptionRequest() {
        try {

            String userId = SettingsPreferences.getConsumer(ctx).getIdUser();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendMySubscriptionRequest(userId , new Callback<MySubscription>() {
                        @Override
                        public void success(MySubscription model, Response response) {

                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("MY_SUBSC_RESPONSE", "" + new Gson().toJson(model));

                                    if (model.getStatus() == 1) {

                                        txt_payed_amount.setText("Paid Amount  : "+model.getPaymentData().getPayedAmount());
                                        txt_payed_date.setText("Start Date  : "+model.getPaymentData().getPaymentDate());
                                        txt_nextpayment_date.setText("End Date  : "+model.getPaymentData().getNextPaymentDate());

//                                        Log.e("MY_SUBSC_RESPONSE", "  "+model.getPaymentData().getPaymentDate()+"    -  "+model.getPaymentData().getNextPaymentDate());
                                    }


                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showWaitIndicator(false);

                            Toast.makeText(ctx, "failure", Toast.LENGTH_LONG).show();

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
