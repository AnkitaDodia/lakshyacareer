package com.lakshyacareer.digitalcoaching.common;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;

import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.model.CategoryData;
import com.lakshyacareer.digitalcoaching.model.Data;
import com.lakshyacareer.digitalcoaching.model.FAQitems;
import com.lakshyacareer.digitalcoaching.model.MaterialsData;
import com.lakshyacareer.digitalcoaching.model.Sample;
import com.lakshyacareer.digitalcoaching.model.SubjectlistData;
import com.lakshyacareer.digitalcoaching.model.TopicListData;
import com.lakshyacareer.digitalcoaching.model.WishlistItems;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by My 7 on 10/25/2017.
 */
public class MasterActivity extends AppCompatActivity {
    private Typeface font;

    ProgressDialog mProgressDialog;

    public static ArrayList<SubjectlistData> SubjectsList = new ArrayList<>();
    public static ArrayList<TopicListData> topicList = new ArrayList<>();
    public static ArrayList<MaterialsData> MaterialsList = new ArrayList<>();
    public static ArrayList<CategoryData> CategoryList = new ArrayList<>();

    public static String subjectId, mText = null, subjectName, topicId, CategoryId, CategoryName;

    public static ArrayList<FAQitems> mFAQitems = new ArrayList<>();
    public static ArrayList<Sample> mSampleList = new ArrayList<>();
    public static ArrayList<WishlistItems> mWishlist = new ArrayList<>();

    public static final int REQUEST_READ_PHONE_STATE = 1;

    public static int isFromTopicListing = 0;

    //if this variable has 0 value then nothing to change if it's fresh install then 1
    public static int isFreshInstall = 1;

    public static String isExpired = "1";
    public static String NextPaymentDate = null;
    public static String ToadyDate = null;
    public static boolean isShowedDialog = false;

    public boolean isRegistered = false;

    public static boolean fromMyDownloads = false, fromTopicView = false;

    public static String dbName = "LakshyCareer.sqlite";

    public static String[] states = {"Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
            "Jammu & Kashmir", "Karnataka", "Kerala", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland",
            "Orissa", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "West Bengal", "Chhattisgarh", "Uttarakhand",
            "Jharkhand", "Telangana"};


//    public static String mFirstName = null;
//    public static String mLastName = null;
//    public static String mDOB = null;
//    public static String mCity = null;
//    public static String mState = null;
//    public static String mEmail = null;
//    public static String mPassword = null;
//    public static String mConfirmPass = null;
//    public static String mMobile = null;
//    public static String IMEI_Number = null;

    public static String mPaymentMode = null;
    public static String PayedAmount = "50";
    public static String PayedDate = null;
    public static String PaymentStatus = null;

//    public String Static_IMEI = getIMEINumber();

    // Code for font type
    public Typeface getBoldTypeFace() {
        font = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaAltBold.ttf");
        return font;
    }

    public Typeface getLightTypeFace() {
        font = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaSemibold.ttf");
        return font;
    }

    public Typeface getRegularTypeFace() {
        font = Typeface.createFromAsset(getAssets(), "fonts/ProximaNovaAltRegular.ttf");
        return font;
    }
    // Code for font type

    // Code for get and set login
    public void setLogin(int i) {
        SharedPreferences sp = getSharedPreferences("LOGIN", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login", i);
        spe.apply();
    }

    public int getLogin() {
        SharedPreferences sp = getSharedPreferences("LOGIN", MODE_PRIVATE);
        int i = sp.getInt("Login", 0);
        return i;
    }
    // Code for get and set login


    // Code for get and set Registration
    public void setRegister(boolean b) {
        SharedPreferences sp = getSharedPreferences("REGISTER", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("Register", b);
        spe.apply();
    }

    public Boolean getRegister() {
        SharedPreferences sp = getSharedPreferences("REGISTER", MODE_PRIVATE);
        Boolean b = sp.getBoolean("Register", false);
        return b;
    }
    // Code for get and set login

    public String GetTodayDate() {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    public String GetExpireNoticeDate(String ExpireDate) {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(ExpireDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        Date newDate = calendar.getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(newDate);

        return formattedDate;
    }


    /*// Code for get and set login
    public void setIsFreshInstall(int i)
    {
        SharedPreferences sp = getSharedPreferences("IsFreshInstall",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("IsFreshInstall",i);
        spe.apply();
        isFreshInstall = i;
    }

    public int getIsFreshInstall()
    {
        SharedPreferences sp = getSharedPreferences("IsFreshInstall",MODE_PRIVATE);
        int i = sp.getInt("IsFreshInstall",1);
        return i;
    }*/


    public void showWaitIndicator(boolean state) {
        showWaitIndicator(state, "");
    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUser(Data user) {
        SettingsPreferences.storeConsumer(this, user);
    }

    public Data getUserDetails() {
        return SettingsPreferences.getConsumer(this);
    }

    public void saveRememberMe(boolean flag) {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putBoolean("REMEMBER_ME", flag);
        spe.commit();
    }

    public boolean getRemember() {
        SharedPreferences sp = getSharedPreferences("REMEMBER", MODE_PRIVATE);
        boolean flag = sp.getBoolean("REMEMBER_ME", false);
        return flag;
    }

    public void saveLoginDetail(String id, String pwd) {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("ID", id);
        spe.putString("PWD", pwd);
        spe.commit();
    }

    public String getLoginId() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String id = sp.getString("ID", "");
        return id;
    }

    public void saveLoginPwd(String pwd) {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("PWD", pwd);
        spe.commit();
    }

    public String getLoginPwd() {
        SharedPreferences sp = getSharedPreferences("LOGIN_Detail", MODE_PRIVATE);
        String pwd = sp.getString("PWD", "");
        return pwd;
    }

    public boolean isAlphaNumeric(String s) {
        boolean isAlphaNumericValue = false;
        boolean alpha = false;
        boolean numeric = false;
        boolean accepted = true;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                numeric = true;
            } else if (Character.isLetter(c)) {
                alpha = true;
            } else {
                accepted = false;
                break;
            }
        }

        if (accepted && alpha && numeric) {
            isAlphaNumericValue = true;
        } else {
            isAlphaNumericValue = false;
        }

        return isAlphaNumericValue;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getIMEINumber() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        String IMEINumber = mngr.getDeviceId();
//        String IMEINumber = "0000000000000000";
        return IMEINumber;
    }

    public String CreateFileName(String filename){
        return filename.replace(" ", "");
    }
}
