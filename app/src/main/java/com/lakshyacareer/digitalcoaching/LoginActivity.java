package com.lakshyacareer.digitalcoaching;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Data;
import com.lakshyacareer.digitalcoaching.model.User;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.ui.UnderlineTextView;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 23-10-2017.
 */

public class LoginActivity extends MasterActivity {

    Context ctx;

    UnderlineTextView txt_signup;

    TextView txt_app_title,txt_tag_login;

    UnderlineTextView txt_forgot_password;

    CheckBox cb_remember_me;

    EditText edt_email,edt_password;

    TextInputLayout input_layout_email,input_layout_password;

    Button btn_login;

    LinearLayout signup_layout;

    private boolean isInternet;

    boolean doubleBackToExitPressedOnce = false;

    String IMEI_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickOnListener();

        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
//            IMEI_number = Static_IMEI;
            Log.e("IMEI_NUMBER" , IMEI_number);
        }
    }

    private void getView() {

        txt_app_title = (TextView) findViewById(R.id.txt_app_title);
        txt_tag_login = (TextView) findViewById(R.id.txt_tag_login);

        txt_forgot_password = (UnderlineTextView) findViewById(R.id.txt_forgot_password);

        input_layout_email = (TextInputLayout) findViewById(R.id.input_layout_email);
        input_layout_password = (TextInputLayout) findViewById(R.id.input_layout_password);

        txt_signup = (UnderlineTextView) findViewById(R.id.txt_signup);

        cb_remember_me = (CheckBox) findViewById(R.id.cb_remember_me);

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);

        btn_login = (Button) findViewById(R.id.btn_login);

        signup_layout = (LinearLayout) findViewById(R.id.signup_layout);

        if(getRemember())
        {
            cb_remember_me.setChecked(true);
            edt_email.setText(getLoginId());
            edt_password.setText(getLoginPwd());
        }
    }

    private void setTypeFace() {

        txt_app_title.setTypeface(getBoldTypeFace());
        txt_tag_login.setTypeface(getBoldTypeFace());
        txt_forgot_password.setTypeface(getLightTypeFace());
        txt_signup.setTypeface(getBoldTypeFace());

        cb_remember_me.setTypeface(getLightTypeFace());

        input_layout_email.setTypeface(getRegularTypeFace());
        input_layout_password.setTypeface(getRegularTypeFace());
        edt_email.setTypeface(getRegularTypeFace());
        edt_password.setTypeface(getRegularTypeFace());

        btn_login.setTypeface(getBoldTypeFace());
    };
                
    private void setClickOnListener()
    {
        signup_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Log.e(TAG, "isRegistered  "+isRegistered);
                if(isRegistered){
                    startActivity(new Intent(ctx, PaymentOptionActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(ctx, SubscribeActivity.class));
                    finish();
                }

//                startActivity(new Intent(ctx, SubscribeActivity.class));
            }
        });

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ctx, ForgotPasswordActivity.class));
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt_email.getText().toString().length() == 0)
                {
                    edt_email.setError("Please enter email");
//                    Toast.makeText(ctx,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                 else if (!isValidEmail(edt_email.getText().toString())) {
                    edt_email.setError("Please enter valid email");
//                    Toast.makeText(ctx,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else if(edt_password.getText().toString().length() == 0)
                {
                    edt_password.setError("Please enter valid password");
//                    Toast.makeText(ctx,"Please enter valid password",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if(!edt_email.getText().toString().equalsIgnoreCase(getLoginId()) ||
                            !edt_password.getText().toString().equalsIgnoreCase(getLoginPwd()) && getRemember())
                    {
                        saveLoginDetail(edt_email.getText().toString(),edt_password.getText().toString());
                    }

                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        cb_remember_me.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    saveRememberMe(true);
                    saveLoginDetail(edt_email.getText().toString(),edt_password.getText().toString());

                    Log.e("Remember","true    :  "+getRemember());
                }
                else
                {
                    saveRememberMe(false);
                    Log.e("Remember","false   :  "+getRemember());
                }
            }
        });

    }

    private void sendLoginRequest()
    {
        try {
            String email = edt_email.getText().toString();
            String password = edt_password.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendLoginRequest(email,password, isFreshInstall, IMEI_number,new Callback<User>() {
                @Override
                public void success(User model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));

                            //if successfully login then status 1 else 0
                            if(model.getStatus()==1){
                                setLogin(1);
                                Data data =  model.getData();
                                updateUser(data);

                                Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();


                                Log.e("Remember"," :  "+getRemember());

                                //is chkIos checked?
                                if (getRemember() == true) {
                                    saveLoginDetail(edt_email.getText().toString(),edt_password.getText().toString());
                                }

                                if(isFromTopicListing == 1)
                                {
                                    isFromTopicListing = 0;

                                    startActivity(new Intent(ctx, TopicViewActivity.class));
                                    finish();
                                }
                                else
                                {
                                    startActivity(new Intent(ctx, NavigationDrawerActivity.class));
                                    finish();
                                }
                            }else{
                                Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    IMEI_number = getIMEINumber();
//                    IMEI_number = Static_IMEI;
                    Log.e("IMEI_NUMBER_OVERRIDE" , IMEI_number);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(ctx , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}