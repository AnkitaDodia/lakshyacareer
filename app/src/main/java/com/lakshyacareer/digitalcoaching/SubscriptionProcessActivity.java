package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;

import java.io.InputStream;

/**
 * Created by abc on 01-11-2017.
 */

public class SubscriptionProcessActivity extends MasterActivity {

    Context ctx;
    WebView webview_sub_process;

    TextView txt_action_title;
    LinearLayout layout_back;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_process);

        ctx = this;

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);


        txt_action_title.setTypeface(getBoldTypeFace());
        txt_action_title.setText("Subscription Process");

        webview_sub_process = (WebView) findViewById(R.id.webview_sub_process);

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       /* String string_sub_process = "JOIN WITH LAKSHYA CAREER ACADEMY AND BE A PART OF THE SUCCESS";
        String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:20px;\"'background-color:transparent' >" + "<p align=\"justify\">" + string_sub_process + "</p>" + "</body></html>";


        webview_sub_process.loadData("<font>" + text + "</font>",
                "text/html; charset=UTF-8", null);*/

        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.subscription_process);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:18px;\"'background-color:transparent' >" + "<p align=\"justify\">" + new String(b) + "</p>" + "</body></html>";

//            String all="<html><body>"+text.replace("\r\n", "<br/>")+"</body></html>";

            webview_sub_process.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);


        } catch (Exception e) {

//            Toast.makeText(ctx, ""+e, Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}