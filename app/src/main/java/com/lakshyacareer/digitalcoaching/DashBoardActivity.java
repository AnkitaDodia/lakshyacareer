package com.lakshyacareer.digitalcoaching;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Data;
import com.lakshyacareer.digitalcoaching.model.User;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.ui.UnderlineTextView;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 21-10-2017.
 */

public class DashBoardActivity extends MasterActivity {

    Context ctx;
    String TAG = "DashBoard";
    UnderlineTextView txt_login_here, txt__subscribe_here;

    TextView txt_welcome_tag, txt_app_title, txt_way_to_tag, txt_already_subscribed, txt_not_subscribed_yet;

    Button btn_start_tutorial;

    LinearLayout lay_start_tutorial, lay_login_here, lay_subscribe_here;

    boolean doubleBackToExitPressedOnce = false;

    String IMEI_number;
    private boolean isInternet;
    String id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();

        if(getLogin() == 1){

            lay_login_here.setVisibility(View.INVISIBLE);
            lay_subscribe_here.setVisibility(View.INVISIBLE);
        }

        id_user = SettingsPreferences.getConsumer(this).getIdUser();
        isRegistered = getRegister();
        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
//            IMEI_number = Static_IMEI;
            Log.e(TAG, "IMEI_NUMBER"+IMEI_number);

            if (isInternet) {
                if(id_user != null && !id_user.isEmpty()){
//                sendUserDetailsRequest();
                    Log.e(TAG ,"If :"+id_user);
                }else {
                    Log.e(TAG ,"Else :"+id_user);
                    sendUserDetailsRequest();
                }

            } else {
                Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }




    }

    private void getView() {

        lay_start_tutorial = (LinearLayout) findViewById(R.id.lay_start_tutorial);

        btn_start_tutorial = (Button) findViewById(R.id.btn_start_tutorial);

        txt_welcome_tag = (TextView) findViewById(R.id.txt_welcome_tag);
        txt_app_title = (TextView) findViewById(R.id.txt_app_title);
        txt_way_to_tag = (TextView) findViewById(R.id.txt_way_to_tag);

        txt_already_subscribed = (TextView) findViewById(R.id.txt_already_subscribed);
        txt_not_subscribed_yet = (TextView) findViewById(R.id.txt_not_subscribed_yet);

        txt_login_here = (UnderlineTextView) findViewById(R.id.txt_login_here);
        txt__subscribe_here = (UnderlineTextView) findViewById(R.id.txt__subscribe_here);

        lay_login_here = (LinearLayout) findViewById(R.id.lay_login_here);
        lay_subscribe_here = (LinearLayout) findViewById(R.id.lay_subscribe_here);
    }

    private void setTypeFace() {

        txt_app_title.setTypeface(getBoldTypeFace());
        txt_welcome_tag.setTypeface(getLightTypeFace());
        txt_way_to_tag.setTypeface(getLightTypeFace());

        txt_login_here.setTypeface(getLightTypeFace());
        txt__subscribe_here.setTypeface(getBoldTypeFace());

        txt_already_subscribed.setTypeface(getLightTypeFace());
        txt_not_subscribed_yet.setTypeface(getLightTypeFace());

        btn_start_tutorial.setTypeface(getBoldTypeFace());
    }

    private void setClickListener()
    {
        txt_login_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ctx, LoginActivity.class));
                finish();
            }
        });

        txt__subscribe_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e(TAG, "isRegistered  "+isRegistered);
                if(isRegistered){
                    startActivity(new Intent(ctx, PaymentOptionActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(ctx, SubscribeActivity.class));
                    finish();
                }
            }
        });

        btn_start_tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ctx, NavigationDrawerActivity.class));
                finish();
            }
        });
    }

    private void sendUserDetailsRequest()
    {
        try {

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendUserDetailsRequest(IMEI_number,new Callback<User>() {
                @Override
                public void success(User model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e(TAG, "USERDETAILS_RESPONSE    - "+ new Gson().toJson(model));

                            //if successfully login then status 1 else 0
                            if(model.getStatus()==1){

                                Data data =  model.getData();
                                updateUser(data);

                                setRegister(true);

//                                Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();


                            }else{
                                setRegister(false);
//                                Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    IMEI_number = getIMEINumber();
//                    IMEI_number = Static_IMEI;
                    Log.e(TAG, "IMEI_NUMBER  "+IMEI_number);

                    if (isInternet) {
                        if(id_user != null && !id_user.isEmpty()){
//                sendUserDetailsRequest();
                            Log.e(TAG,"If :");
                        }else {
                            Log.e(TAG,"Else :");
                            sendUserDetailsRequest();
                        }

                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(ctx , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}