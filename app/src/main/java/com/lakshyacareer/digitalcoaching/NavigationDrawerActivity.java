package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.fragment.BrowseSubjectFragment;
import com.lakshyacareer.digitalcoaching.fragment.MaterialsFragment;
import com.lakshyacareer.digitalcoaching.fragment.MyAccountFragment;
import com.lakshyacareer.digitalcoaching.fragment.SettingsFragment;
import com.lakshyacareer.digitalcoaching.fragment.SubjectsDownloadFragment;
import com.lakshyacareer.digitalcoaching.fragment.WishlistFragment;
import com.lakshyacareer.digitalcoaching.model.MySubscription;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.CustomTypefaceSpan;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 26-10-2017.
 */

public class NavigationDrawerActivity extends MasterActivity {

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_ACCOUNT = "account";
    private static final String TAG_DOWNLOAD = "download";
    private static final String TAG_MATERIALS = "materials";
    private static final String TAG_WISHLIST = "wishlist";
    private static final String TAG_BROWSE_SUBJECT = "browse subject";
    private static final String TAG_SETTINGS = "settings";

    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    public Toolbar toolbar;
    Context ctx;
    Fragment currentFragment;
    //For exit from application
    boolean doubleBackToExitPressedOnce = false;
    TextView mTitle;
    String DialogMsg = null;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private View navHeader;
    private TextView txtName, txtWebsite;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    private boolean isInternet;
    private boolean isExpiredDialog = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        ctx = this;
        isInternet = new InternetStatus().isInternetOn(ctx);

        mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        mTitle.setTypeface(getBoldTypeFace());

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        if (getLogin() == 1) {
            if (isInternet) {
                //before live
//                sendMySubscriptionRequest();
            } else {
                Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
            }

        }

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_BROWSE_SUBJECT;
            changeFragment(new BrowseSubjectFragment());
            navigationView.setCheckedItem(R.id.nav_browse_subject);
            mTitle.setText(activityTitles[navItemIndex]);
        }

//        navView = (NavigationView) findViewById(R.id.navView);
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        ChangeNavItems();
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {

        txtName.setTypeface(getRegularTypeFace());
        txtWebsite.setTypeface(getRegularTypeFace());


        if (getLogin() == 1) {
            String firstName = SettingsPreferences.getConsumer(this).getFirstName();
            String lastName = SettingsPreferences.getConsumer(this).getLastName();

            firstName = firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase();
            lastName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1).toLowerCase();

            String name = firstName + " " + lastName;
            txtName.setText(name);
            txtWebsite.setText(SettingsPreferences.getConsumer(this).getEmail());
        } else {
            txtName.setText(R.string.title_name);
            txtWebsite.setText("https://lakshyacareer.in/");
        }
    }

    private void setToolbarTitle() {
        mTitle.setText(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:

                        // launch new intent instead of loading fragment
                        startActivity(new Intent(ctx, DashBoardActivity.class));
                        break;
                    case R.id.nav_account:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_ACCOUNT;
                        changeFragment(new MyAccountFragment());

                        break;
                    case R.id.nav_downloads:
//                        navItemIndex = 4;
//                        CURRENT_TAG = TAG_DOWNLOAD;
//                        changeFragment(new SubjectsDownloadFragment());

//                        navigationView.setCheckedItem(R.id.nav_browse_subject);

                        startActivity(new Intent(ctx, DownloadsTabsActivity.class));

                        break;
                    case R.id.nav_wishlist:
                        MasterActivity.mWishlist.clear();

                        navItemIndex = 5;
                        CURRENT_TAG = TAG_WISHLIST;
                        changeFragment(new WishlistFragment());
                        break;
                    case R.id.nav_browse_subject:

                        navItemIndex = 2;
                        CURRENT_TAG = TAG_BROWSE_SUBJECT;
                        MasterActivity.SubjectsList.clear();
                        changeFragment(new BrowseSubjectFragment());

                        break;
                    case R.id.nav_settings:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_SETTINGS;
                        changeFragment(new SettingsFragment());
                        break;
                    case R.id.nav_materials:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_MATERIALS;
                        changeFragment(new MaterialsFragment());
                        break;
                    case R.id.nav_logout:

                        SettingsPreferences.clearDB(NavigationDrawerActivity.this);

                        setLogin(0);

                        isFreshInstall = 0;
                        isFromTopicListing = 0;

                        startActivity(new Intent(NavigationDrawerActivity.this, LoginActivity.class));
                        finish();
                        break;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                drawer.closeDrawers();

                // set toolbar title
                setToolbarTitle();
                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", getBoldTypeFace()), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void changeFragment(Fragment targetFragment) {
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame, targetFragment, targetFragment.getClass().getName());
        transaction.addToBackStack(targetFragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = targetFragment;
    }

    public void ChangeNavItems() {
        if (getLogin() == 1) {

            navigationView.getMenu().findItem(R.id.nav_account).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_downloads).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_wishlist).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
//            Log.e("ChangeNavItems", "if else  :"+getLogin()+"  "+isExpired);

//            if(isExpired.equals("1")){
//
//                navigationView.getMenu().findItem(R.id.nav_account).setVisible(false);
//                navigationView.getMenu().findItem(R.id.nav_downloads).setVisible(false);
//                navigationView.getMenu().findItem(R.id.nav_wishlist).setVisible(false);
//                navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
//                Log.e("ChangeNavItems", "if if  :"+getLogin()+"  "+isExpired);
//            }
//            else{
//                navigationView.getMenu().findItem(R.id.nav_account).setVisible(true);
//                navigationView.getMenu().findItem(R.id.nav_downloads).setVisible(true);
//                navigationView.getMenu().findItem(R.id.nav_wishlist).setVisible(true);
//                navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
//                Log.e("ChangeNavItems", "if else  :"+getLogin()+"  "+isExpired);
//            }

        } else {

            navigationView.getMenu().findItem(R.id.nav_account).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_downloads).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_wishlist).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        }
    }

    @Override
    public void onBackPressed() {

//        FragmentManager manager = getFragmentManager();
//        if (manager.getBackStackEntryCount() > 0) {
//            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
//            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }

//        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

//        if (mFragmentManager.getBackStackEntryCount() > 0)
//            mFragmentManager.popBackStackImmediate();
//        else super.onBackPressed();

        Log.e("Tag", "NavOnBack");

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        } else {
            if (doubleBackToExitPressedOnce) {

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                System.exit(1);
            }

            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (fromMyDownloads) {
            fromMyDownloads = false;
//            navigationView.setCheckedItem(R.id.nav_browse_subject);
            mTitle.setText(activityTitles[navItemIndex]);
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
        }
        ChangeNavItems();
        loadNavHeader();
    }

    private void sendMySubscriptionRequest() {
        try {

            String userId = SettingsPreferences.getConsumer(ctx).getIdUser();

//            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendMySubscriptionRequest(userId, new Callback<MySubscription>() {
                @Override
                public void success(MySubscription model, Response response) {

//                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("MY_SUBSC_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus() == 1) {

//                                showWaitIndicator(false);

                                isExpired = model.getPaymentData().getExpired();
                                NextPaymentDate = model.getPaymentData().getNextPaymentDate();
                                ToadyDate = model.getPaymentData().getTodayDate();

                                ExpireNotification();


                                if (isExpired.equals("1")) {

                                    SettingsPreferences.clearDB(NavigationDrawerActivity.this);

                                    setLogin(0);

                                    isFromTopicListing = 0;

                                    DialogMsg = "Your plan has been expired, please renew to continue using our service. Thank you!!";
                                    isExpiredDialog = true;
                                    openDialog(DialogMsg);


                                }


                                Log.e("MY_SUBSC_RESPONSE", "  " + model.getPaymentData().getExpired() + "    -  " + model.getPaymentData().getNextPaymentDate());
                            }


                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }

                    ChangeNavItems();
                    loadNavHeader();

                }

                @Override
                public void failure(RetrofitError error) {
//                    showWaitIndicator(false);

                    Toast.makeText(ctx, "failure", Toast.LENGTH_LONG).show();

                    Log.e("ERROR", "" + error.getMessage());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ExpireNotification() {

        Log.e("NextPaymentDate", "   :  " + GetExpireNoticeDate(NextPaymentDate));
        Log.e("ToadyDate", "   :  " + ToadyDate);


        if (GetExpireNoticeDate(NextPaymentDate).equals(ToadyDate)) {
//            Toast.makeText(ctx, "You will fired in few days", Toast.LENGTH_LONG).show();

            if(!isShowedDialog){

                DialogMsg = "Your Subscription Will Expire in 3 Days, please renew before " + NextPaymentDate + " to continue using our service. Thank you!!";
                isExpiredDialog = false;
                openDialog(DialogMsg);
                isShowedDialog = true;

            }

        }

        if (ToadyDate.equals(NextPaymentDate)) {

            if(!isShowedDialog){

                DialogMsg = "Your subscription will expire soon, please renew to continue using our service. Thank you!!";
                isExpiredDialog = false;
                openDialog(DialogMsg);
                isShowedDialog = true;
            }


        }
    }

    public void openDialog(String DialogMsg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Lakshya Career");
        alertDialogBuilder.setMessage(DialogMsg).setCancelable(false)
                .setPositiveButton("RENEW", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        ctx.isFromTopicListing = 1;
//                        ctx.startActivity(new Intent(ctx, LoginActivity.class));

                    }
                }).setNegativeButton("Remind Me Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                if(isExpiredDialog){
                    startActivity(new Intent(NavigationDrawerActivity.this, DashBoardActivity.class));
                    finish();
                }

                dialog.dismiss();
            }
        }).show();
    }
}
