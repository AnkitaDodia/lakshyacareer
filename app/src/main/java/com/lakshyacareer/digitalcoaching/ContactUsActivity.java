package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.ContactUs;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 30-10-2017.
 */

public class ContactUsActivity extends MasterActivity {

    Context ctx;

    private LinearLayout layout_back;
    private TextView txt_action_title;

    private TextInputLayout input_layout_name, input_layout_email, input_layout_mobile, input_layout_city, input_layout_subject, input_layout_message;
    private EditText edit_name, edit_email, edit_mobile, edit_city, edit_subject, edit_message;

    private Button btn_contactus_submit;
    private boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_us);

        ctx = this;
        isInternet = new InternetStatus().isInternetOn(ctx);

        initViews();
        setClickListener();
    }

    private void initViews(){

        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_action_title = (TextView)findViewById(R.id.txt_action_title);

        input_layout_name = (TextInputLayout) findViewById(R.id.input_layout_name);
        input_layout_email = (TextInputLayout) findViewById(R.id.input_layout_email);
        input_layout_mobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        input_layout_city = (TextInputLayout) findViewById(R.id.input_layout_city);
        input_layout_subject = (TextInputLayout) findViewById(R.id.input_layout_subject);
        input_layout_message = (TextInputLayout) findViewById(R.id.input_layout_message);

        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_email= (EditText) findViewById(R.id.edit_email);
        edit_mobile = (EditText) findViewById(R.id.edit_mobile);
        edit_city = (EditText) findViewById(R.id.edit_city);
        edit_subject = (EditText) findViewById(R.id.edit_subject);
        edit_message = (EditText) findViewById(R.id.edit_message);

        input_layout_name.setTypeface(getRegularTypeFace());
        input_layout_email.setTypeface(getRegularTypeFace());
        input_layout_mobile.setTypeface(getRegularTypeFace());
        input_layout_city.setTypeface(getRegularTypeFace());
        input_layout_subject.setTypeface(getRegularTypeFace());
        input_layout_message.setTypeface(getRegularTypeFace());

        edit_name.setTypeface(getRegularTypeFace());
        edit_email.setTypeface(getRegularTypeFace());
        edit_mobile.setTypeface(getRegularTypeFace());
        edit_city.setTypeface(getRegularTypeFace());
        edit_subject.setTypeface(getRegularTypeFace());
        edit_message.setTypeface(getRegularTypeFace());

        btn_contactus_submit = (Button)findViewById(R.id.btn_contactus_submit);
        btn_contactus_submit.setTypeface(getBoldTypeFace());

        txt_action_title.setTypeface(getBoldTypeFace());
        txt_action_title.setText("Contact Us");
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_contactus_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edit_name.getText().toString().length() == 0)
                {
                    edit_name.setError("Please enter name");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }
                else if(edit_email.getText().toString().length() == 0)
                {
                    edit_email.setError("Please enter email");
//                    Toast.makeText(ctx,"Please enter email",Toast.LENGTH_LONG).show();

                }else if (!isValidEmail(edit_email.getText().toString())) {

                    edit_email.setError("Please enter valid email");
//                    Toast.makeText(ctx,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else if(edit_mobile.getText().toString().length() == 0)
                {
                    edit_mobile.setError("Please enter mobile number");
//                    Toast.makeText(ctx,"Please enter mobile number",Toast.LENGTH_LONG).show();
                }
                else if(edit_city.getText().toString().length() == 0)
                {
                    edit_city.setError("Please define your city");
//                    Toast.makeText(ctx,"Please define your city",Toast.LENGTH_LONG).show();
                }
                else if(edit_subject.getText().toString().length() == 0)
                {
                    edit_subject.setError("Please enter subject");
//                    Toast.makeText(ctx,"Please enter subject",Toast.LENGTH_LONG).show();
                }
                else if(edit_message.getText().toString().length() == 0)
                {
                    edit_message.setError("Please enter message");
//                    Toast.makeText(ctx,"Please enter message",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendContactUsRequest();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void sendContactUsRequest() {
        try {
            String name = edit_name.getText().toString();
            String email = edit_email.getText().toString();
            String mobile = edit_mobile.getText().toString();
            String city = edit_city.getText().toString();
            String subject = edit_subject.getText().toString();
            String message = edit_message.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendContactUsRequest(name,email,mobile,city,subject,message
                    ,new Callback<ContactUs>() {
                        @Override
                        public void success(ContactUs model, Response response) {
                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("CONTACT_US_RESPONSE", "" + new Gson().toJson(model));

                                    Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();

                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error)
                        {
                            showWaitIndicator(false);

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}