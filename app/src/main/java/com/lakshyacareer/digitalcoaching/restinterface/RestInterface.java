package com.lakshyacareer.digitalcoaching.restinterface;

import com.lakshyacareer.digitalcoaching.model.AboutUs;
import com.lakshyacareer.digitalcoaching.model.AddDownload;
import com.lakshyacareer.digitalcoaching.model.Category;
import com.lakshyacareer.digitalcoaching.model.ChangePassword;
import com.lakshyacareer.digitalcoaching.model.ContactUs;
import com.lakshyacareer.digitalcoaching.model.DeleteVideo;
import com.lakshyacareer.digitalcoaching.model.FAQData;
import com.lakshyacareer.digitalcoaching.model.ForgotPassword;
import com.lakshyacareer.digitalcoaching.model.Materials;
import com.lakshyacareer.digitalcoaching.model.MyDownload;
import com.lakshyacareer.digitalcoaching.model.MySubscription;
import com.lakshyacareer.digitalcoaching.model.PaymentInfo;
import com.lakshyacareer.digitalcoaching.model.PaytmChecksum;
import com.lakshyacareer.digitalcoaching.model.RemoveWishList;
import com.lakshyacareer.digitalcoaching.model.SampleData;
import com.lakshyacareer.digitalcoaching.model.SubjectList;
import com.lakshyacareer.digitalcoaching.model.TopicList;
import com.lakshyacareer.digitalcoaching.model.TopicPlay;
import com.lakshyacareer.digitalcoaching.model.TopicView;
import com.lakshyacareer.digitalcoaching.model.User;
import com.lakshyacareer.digitalcoaching.model.WishList;
import com.lakshyacareer.digitalcoaching.model.WishlistData;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by Adite-Ankita on 6/30/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "http://api.lakshyacareer.in";

    String LOGIN = "/users/login";
    String REGISTER = "/users/useradd";
    String FORGOT_PASSWORD = "/users/forgotpassword";
    String CONTACT_US = "/users/contactusadd";
    String CHANGE_PASSWORD = "/users/changepassword";
    String EDIT_PROFILE = "/users/useredit";
    String FAQ_LIST = "/users/faqlist";
    String ABOUTUS = "/home/aboutus";
    String SUBJECT_LIST = "/subjects/subjectlist";
    String MATERIALS_LIST = "/materials/categoryList";
    String CATEGORY_LIST = "/materials/categoryWise";
    String TOPIC_LIST = "/topics/topiclist";
    String ADD_FAVORITE = "/topics/addtowishlist";
    String REMOVE_FAVORITE = "/topics/removefromwishlist";
    String SAMPLE_TUTORIALS = "/topics/samplevideolist";
    String TOPIC_VIEW = "/topics/topicview";
    String TOPIC_PLAY = "/topics/topicplay";
    String WISHLIST = "/topics/mywishlist";
    String ADD_DOWNLOAD = "/topics/topicdownload";
    String MY_DOWNLOAD = "/topics/mydownloads";
    String PAYTM_CHECKSUM = "/home/generate";
    String DELETE_DOWNLOAD ="/topics/videodelete";
    String MY_SUBSCRIPTION = "/payment/paymentinfo";
    String PAYMENTINFO = "/payment/updatepaymentinfo";
    String USERDETAILS = "/users/userdetail";



    //LogIn
    @FormUrlEncoded
    @POST(LOGIN)
    public void sendLoginRequest(@Field("email") String email, @Field("password") String password, @Field("isFreshInstall") int isFreshInstall,
                                 @Field("IMEI_number") String IMEI_number ,Callback<User> callBack);

    //Registration
    @FormUrlEncoded
    @POST(REGISTER)
    public void sendRegisterRequest(@Field("first_name") String first_name, @Field("last_name") String last_name,
                                    @Field("dob") String dtae_of_birth, @Field("email") String email, @Field("mobile_number") String mobile_number,
                                    @Field("password") String password,@Field("confirm_password") String confirm_password,
                                    @Field("state") String state,@Field("city") String city,
                                    @Field("IMEI_number") String IMEI_number,
                                    Callback<User> callBack);

    //Forgot Password
    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    public void sendForgotPasswordRequest(@Field("email") String email, Callback<ForgotPassword> callBack);

    //ContactUs
    @FormUrlEncoded
    @POST(CONTACT_US)
    public void sendContactUsRequest(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("city") String city,
                                    @Field("state") String state,
                                    @Field("message") String message,
                                    @Field("mobile_number") String mobile_number,
                                    Callback<ContactUs> callBack);


    // Content management
    @GET(ABOUTUS)
    public void getAboutUs(Callback<AboutUs> callback);

    // getHomePage
    @GET(SUBJECT_LIST)
    public void getSubjectListRequest(Callback<SubjectList> callback);

    // getHomePage
    @GET(MATERIALS_LIST)
    public void getMaterialsRequest(Callback<Materials> callback);


    @FormUrlEncoded
    @POST(CATEGORY_LIST)
    public void getCategotyRequest(@Field("id_category") String id_subject,Callback<Category> callBack);

    // getHomePage
    @GET(FAQ_LIST)
    public void getFAQListRequest(Callback<FAQData> callback);

    //Get Topic List
    @FormUrlEncoded
    @POST(TOPIC_LIST)
    public void sendTopicRequest(@Field("id_subject") String id_subject,Callback<TopicList> callBack);

    //Registration
    @FormUrlEncoded
    @POST(EDIT_PROFILE)
    public void sendChangeProfileRequest(@Field("id_user") String user_id,
                                    @Field("first_name") String first_name, @Field("last_name") String last_name,
                                    @Field("dob") String dtae_of_birth,
                                    @Field("email") String email,@Field("mobile_number") String mobile_number,
                                    @Field("password") String password,@Field("confirm_password") String confirm_password,
                                    @Field("state") String state,@Field("city") String city,
                                    @Field("payment_mode") String payment_mode, @Field("IMEI_number") String IMEI_number
                                    ,Callback<User> callBack);

    //Change Password
    @FormUrlEncoded
    @POST(CHANGE_PASSWORD)
    public void sendChangePasswordRequest(@Field("id_user") String user_id,
                                          @Field("password") String password,
                                          @Field("new_password") String new_password,
                                          @Field("confirm_password") String confirm_password,
                                          Callback<ChangePassword> callBack);

    //ADD TO FAVORITE
    @FormUrlEncoded
    @POST(ADD_FAVORITE)
    public void AddWishListRequest(@Field("id_user") String id_user, @Field("id_topic") String id_topic,Callback<WishList> callBack);

    //ADD TO FAVORITE
    @FormUrlEncoded
    @POST(REMOVE_FAVORITE)
    public void RemoveWishListRequest(@Field("id_topic_wishlist") String id_topic_wishlist, Callback<RemoveWishList> callBack);

    // Content management
    @GET(SAMPLE_TUTORIALS)
    public void getSampleTutorials(Callback<SampleData> callback);

    //TOPIC VIEW
    @FormUrlEncoded
    @POST(TOPIC_VIEW)
    public void topicViewRequest(@Field("id_user") String id_user, @Field("id_topic") String id_topic,Callback<TopicView> callBack);

    //TOPIC PLAY
    @FormUrlEncoded
    @POST(TOPIC_PLAY)
    public void topicPlayRequest(@Field("id_user") String id_user, @Field("id_topic") String id_topic,Callback<TopicPlay> callBack);

    //WISHLIST
    @FormUrlEncoded
    @POST(WISHLIST)
    public void sendWishlistRequest(@Field("id_user") String id_user,Callback<WishlistData> callBack);

    //ADD DOWNLOAD
    @FormUrlEncoded
    @POST(ADD_DOWNLOAD)
    public void addDownloadRequest(@Field("id_user") String id_user, @Field("id_topic") String id_topic,Callback<AddDownload> callBack);

    //TOPIC PLAY
    @FormUrlEncoded
    @POST(MY_DOWNLOAD)
    public void myDownloadRequest(@Field("id_user") int id_user,Callback<MyDownload> callBack);

    //PaytmChecksum
    @FormUrlEncoded
    @POST(PAYTM_CHECKSUM)
    public void sendPaytmChechsumRequest(@Field("MID") String first_name,
                                         @Field("ORDER_ID") String last_name,
                                         @Field("CUST_ID") String dtae_of_birth,
                                         @Field("INDUSTRY_TYPE_ID") String email,
                                         @Field("CHANNEL_ID") String mobile_number,
                                         @Field("TXN_AMOUNT") String password,
                                         @Field("WEBSITE") String confirm_password,
                                         @Field("CALLBACK_URL") String state,
                                         @Field("EMAIL") String city,
                                         @Field("MOBILE_NO") String payment_mode,
                                         Callback<PaytmChecksum> callBack);

    //DELETE VIDEO
    @FormUrlEncoded
    @POST(DELETE_DOWNLOAD)
    public void sendDeleteDownloadedRequest(@Field("id_my_download") String  id_my_download,Callback<DeleteVideo> callBack);

    //MY_SUBSCRIPTION
    @FormUrlEncoded
    @POST(MY_SUBSCRIPTION)
    public void sendMySubscriptionRequest(@Field("id_user") String  id_user,Callback<MySubscription> callBack);

    //MY_SUBSCRIPTION
    @FormUrlEncoded
    @POST(PAYMENTINFO)
    public void sendPaymentInfoRequest(@Field("id_user") String  id_user,
                                       @Field("payment_status") String  payment_status,
                                       @Field("payed_amount") String  payed_amount,
                                       @Field("payment_date") String  payment_date,
                                       @Field("payment_mode") String  payment_mode,
                                       Callback<PaymentInfo> callBack);


    //Registration
    @FormUrlEncoded
    @POST(USERDETAILS)
    public void sendUserDetailsRequest(@Field("IMEI_number") String IMEI_number,
                                         Callback<User> callBack);
}
