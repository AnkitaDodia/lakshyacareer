package com.lakshyacareer.digitalcoaching.fragment;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.BuildConfig;
import com.lakshyacareer.digitalcoaching.DownloadsTabsActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.VideoPlayerActivity;
import com.lakshyacareer.digitalcoaching.model.DeleteVideo;
import com.lakshyacareer.digitalcoaching.model.MaterialsDownloads;
import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DBMaterialsHelper;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 29-10-2017.
 */

public class MaterialsDownloadFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    DownloadsTabsActivity mContext;
//    Context mContext;

    TextView error_msg;

    LinearLayout error_layout_topic;

    private boolean isInternet;

    ListView list_mydownloads;

    private ArrayList<MaterialsDownloads> mMaterialsDownloads = new ArrayList<>();

    String msg;

    private DBMaterialsHelper mDbHelper, dbAdapters;
    private SQLiteDatabase mDb;
    static int version_val = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (DownloadsTabsActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        initView(view);
        setTypeFace();
        setClickListener();

        mDbHelper = new DBMaterialsHelper(mContext, "LakshyCareer.sqlite", null,version_val);
        dbAdapters = DBMaterialsHelper.getDBAdapterInstance(mContext);
        try {
            dbAdapters.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setListData();


    }

    private void initView(View view) {
//        txt_action_title = (TextView) view.findViewById(R.id.txt_action_title);
        error_msg = (TextView) view.findViewById(R.id.error_msg);

//        layout_back = (LinearLayout) view.findViewById(R.id.layout_back);
        error_layout_topic = (LinearLayout) view.findViewById(R.id.error_layout);

        list_mydownloads = (ListView) view.findViewById(R.id.list_mydownloads);

//        txt_action_title.setText("My Download");
    }

    private void setTypeFace()
    {
//        txt_action_title.setTypeface(mContext.getBoldTypeFace());
        error_msg.setTypeface(mContext.getLightTypeFace());
    }

    private void setClickListener()
    {
        mContext.fromMyDownloads = true;
//        layout_back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                mContext.finish();
//            }
//        });
    }

    private void setListData()
    {
        try {
            dbAdapters.openDataBase();
            mMaterialsDownloads = dbAdapters.GetMaterials();
            dbAdapters.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("SIZE",""+mMaterialsDownloads.size());

        if(mMaterialsDownloads.size() > 0)
        {
            error_layout_topic.setVisibility(View.GONE);
            list_mydownloads.setVisibility(View.VISIBLE);


            DownloadListAdapter mDownloadListAdapter= new DownloadListAdapter(mContext, mMaterialsDownloads);
            list_mydownloads.setAdapter(mDownloadListAdapter);
        }
        else
        {
            error_layout_topic.setVisibility(View.VISIBLE);
            list_mydownloads.setVisibility(View.GONE);
            error_msg.setText("You haven't downloaded anything!!");
        }
    }

    public class DownloadListAdapter extends BaseAdapter
    {
        ArrayList<MaterialsDownloads> mMaterialsDownloadsList = new ArrayList<>();

        DownloadsTabsActivity mContext;

        DownloadListAdapter.MyDownloadHolder holder;

        public DownloadListAdapter(DownloadsTabsActivity context, ArrayList<MaterialsDownloads> mMaterialsDownloads)
        {
            mContext = context;
            mMaterialsDownloadsList = mMaterialsDownloads;
        }

        @Override
        public int getCount() {
            return mMaterialsDownloadsList.size();
        }

        @Override
        public Object getItem(int position) {
            return mMaterialsDownloadsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            holder = null;
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.row_material_downloads, null);
                holder = new DownloadListAdapter.MyDownloadHolder();


                holder.txt_number_download = (TextView) convertView.findViewById(R.id.txt_number_download);
                holder.txt_material_download = (TextView) convertView.findViewById(R.id.txt_material_download);
                holder.txt_category_download = (TextView) convertView.findViewById(R.id.txt_category_download);
                holder.txt_size_download = (TextView) convertView.findViewById(R.id.txt_size_download);

                holder.img_open_downloads = (ImageView) convertView.findViewById(R.id.img_open_downloads);
                holder.lay_open_downloads = (LinearLayout) convertView.findViewById(R.id.lay_open_downloads);
                holder.img_delete_downloads = (ImageView) convertView.findViewById(R.id.img_delete_downloads);
                holder.lay_delete_downloads = (LinearLayout) convertView.findViewById(R.id.lay_delete_downloads);

                convertView.setTag(holder);
            } else {

                holder = (DownloadListAdapter.MyDownloadHolder) convertView.getTag();
            }

            final MaterialsDownloads mMaterialsDownloads = mMaterialsDownloadsList.get(i);


            int pos = i + 1;
            holder.txt_number_download.setText(String.valueOf(pos));


            holder.txt_number_download.setTypeface(mContext.getBoldTypeFace());
            holder.txt_material_download.setTypeface(mContext.getBoldTypeFace());
            holder.txt_category_download.setTypeface(mContext.getRegularTypeFace());
            holder.txt_size_download.setTypeface(mContext.getRegularTypeFace());

            holder.txt_material_download.setText(mMaterialsDownloads.getMaterialName());
            holder.txt_category_download.setText(mMaterialsDownloads.getMaterialCategoryName());
            holder.txt_size_download.setText(mMaterialsDownloads.getMaterialSize());

            holder.lay_open_downloads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    File sharedFile = new File(mMaterialsDownloadsList.get(i).getMaterialPath());
                    Intent mPdfIntent = new Intent(Intent.ACTION_VIEW);
                    mPdfIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID+ ".provider", sharedFile);
                    mPdfIntent.setDataAndType(uri, "application/pdf");

                    ///storage/emulated/0/Android/data/com.lakshyacareer.digitalcoaching/files/Materials/Category/EZHm7sQKv4.pdf
                    Log.e("MaterialsDownloads", ""+mMaterialsDownloadsList.get(i).getMaterialPath());
//                    File file = new File(mMaterialsDownloadsList.get(i).getMaterialPath());
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(mPdfIntent);
                }
            });

            holder.lay_delete_downloads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dbAdapters.openDataBase();
                        dbAdapters.deleteRow(mMaterialsDownloadsList.get(i).getMaterialId());
                        dbAdapters.close();

                        File fdelete = new File(mMaterialsDownloadsList.get(i).getMaterialPath());
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                System.out.println("file Deleted :" + mMaterialsDownloadsList.get(i).getMaterialPath());
                            } else {
                                System.out.println("file not Deleted :" + mMaterialsDownloadsList.get(i).getMaterialPath());
                            }
                        }
//                        System.out.println("file not Deleted :" + mMaterialsDownloadsList.get(i).getVideo_topic_download_id());
//                        sendVideoDeleteRequest(mMaterialsDownloadsList.get(i).getVideo_topic_download_id());
                        notifyDataSetChanged();
                        setListData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            return convertView;
        }

        class MyDownloadHolder {

            TextView txt_number_download, txt_material_download, txt_category_download, txt_size_download;
            ImageView img_open_downloads, img_delete_downloads;
            LinearLayout lay_open_downloads, lay_delete_downloads;
        }


    }

}
