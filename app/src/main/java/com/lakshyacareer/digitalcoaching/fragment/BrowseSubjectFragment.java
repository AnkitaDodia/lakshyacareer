package com.lakshyacareer.digitalcoaching.fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapter.SubjectListAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.SubjectList;
import com.lakshyacareer.digitalcoaching.model.SubjectlistData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 30-10-2017.
 */

public class BrowseSubjectFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    NavigationDrawerActivity mContext;

    private boolean isInternet;

    ListView SubjectList;

    private ArrayList<SubjectlistData> SubjectsList = new ArrayList<>();

    private SubjectListAdapter mSubjectListAdapter;

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_browsesubject, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        mContext = (NavigationDrawerActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView(view);

        if(MasterActivity.SubjectsList.size() > 0)
        {
            SubjectsList = MasterActivity.SubjectsList;
            setListData();
        }
        else
        {
            if (isInternet) {
                getSubjectListRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void inItView(View view) {
        SubjectList = (ListView)view.findViewById(R.id.list);
    }

    private void setListData() {
        mSubjectListAdapter = new SubjectListAdapter(mContext, SubjectsList);
        SubjectList.setAdapter(mSubjectListAdapter);
    }

    private void getSubjectListRequest() {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getSubjectListRequest(new Callback<SubjectList>() {
                @Override
                public void success(SubjectList model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SUBJECT_LIST_RESPONSE", "" + new Gson().toJson(model));

                            SubjectsList = model.getSubjectlistData();

                            MasterActivity.SubjectsList = SubjectsList;

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*@Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        System.exit(1);
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(mContext , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                    return true;
                }
                return false;
            }
        });
    }*/
}
