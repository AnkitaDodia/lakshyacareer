package com.lakshyacareer.digitalcoaching.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;

/**
 * Created by abc on 29-10-2017.
 */

public class MyDownloadFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    NavigationDrawerActivity mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (NavigationDrawerActivity) getActivity();

        setHasOptionsMenu(true);

        mContext.toolbar.setNavigationIcon(R.drawable.ic_back);
        mContext.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
            }
        });
    }

    private void inItView(View view) {
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
                    return true;
                }
                return false;
            }
        });
    }
}
