package com.lakshyacareer.digitalcoaching.fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.adapter.MaterialsListAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Materials;
import com.lakshyacareer.digitalcoaching.model.MaterialsData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 30-10-2017.
 */

public class MaterialsFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    NavigationDrawerActivity mContext;

    private boolean isInternet;

    ListView ListMaterial;

    private ArrayList<MaterialsData> mMaterialsData = new ArrayList<>();

    private MaterialsListAdapter mMaterialsListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_browsesubject, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        mContext = (NavigationDrawerActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView(view);

        if(MasterActivity.MaterialsList.size() > 0)
        {
            mMaterialsData = MasterActivity.MaterialsList;
            setListData();
        }
        else
        {
            if (isInternet) {
                getMaterialsRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void inItView(View view) {
        ListMaterial = (ListView)view.findViewById(R.id.list);
    }

    private void setListData() {
        mMaterialsListAdapter = new MaterialsListAdapter(mContext, mMaterialsData);
        ListMaterial.setAdapter(mMaterialsListAdapter);
    }

    private void getMaterialsRequest() {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getMaterialsRequest(new Callback<Materials>() {
                @Override
                public void success(Materials model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("MATERIALS_LIST_RESPONSE", "" + new Gson().toJson(model));

                            mMaterialsData = model.getData();

                            MasterActivity.MaterialsList = mMaterialsData;

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
