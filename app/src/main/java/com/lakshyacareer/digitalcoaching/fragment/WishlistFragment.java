package com.lakshyacareer.digitalcoaching.fragment;

import android.app.Fragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.MyDownloadsActivity;
import com.lakshyacareer.digitalcoaching.NavigationDrawerActivity;
import com.lakshyacareer.digitalcoaching.R;
import com.lakshyacareer.digitalcoaching.VideoPlayerActivity;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.AddDownload;
import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;
import com.lakshyacareer.digitalcoaching.model.RemoveWishList;
import com.lakshyacareer.digitalcoaching.model.TopicPlay;
import com.lakshyacareer.digitalcoaching.model.WishList;
import com.lakshyacareer.digitalcoaching.model.WishlistData;
import com.lakshyacareer.digitalcoaching.model.WishlistItems;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadManager;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 29-10-2017.
 */

public class WishlistFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    NavigationDrawerActivity mContext;

    TextView error_msg,mProgressTxt;

    LinearLayout layout_back,error_layout_topic;

    private boolean isInternet;

    ListView list_wishlist;

    private ArrayList<WishlistItems> mWishlist = new ArrayList<>();
    String msg;

    //Code for download
    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;

    MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();

    DownloadRequest downloadRequest;
    int downloadId;

    ProgressBar mProgress;

    public String APP_PATH_SD_CARD = "/Subjects/";

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder build;
    int id = 1;
    //Code for download

    //Code for database
    DatabaseHelper mDbHelper, dbAdapters;
    SQLiteDatabase mDb;
    static int version_val = 1;
    MyDownloadsList modelDownload;
    Uri destinationUri;

    String DestinationPath ,TOPIC_NAME,TOPIC_ID,TOPIC_DURATION, TOPIC_URL, IdTopicWishlist;
    private ArrayList<MyDownloadsList> mMyDownloadlist = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wishlist, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (NavigationDrawerActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        setHasOptionsMenu(true);

        mContext.toolbar.setNavigationIcon(R.drawable.ic_back);
        mContext.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
            }
        });

        initView(view);
        setTypeFace();
        setupForDB();

        if(MasterActivity.mWishlist.size() > 0)
        {
            mWishlist = MasterActivity.mWishlist;
            setListData();
        }
        else
        {
            if (isInternet) {
                getWishlistRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initView(View view) {
        error_msg = (TextView) view.findViewById(R.id.error_msg);

        layout_back = (LinearLayout) view.findViewById(R.id.layout_back);
        error_layout_topic = (LinearLayout) view.findViewById(R.id.error_layout);

        list_wishlist = (ListView) view.findViewById(R.id.list_wishlist);

        mProgress = (ProgressBar) view.findViewById(R.id.prgbr_download);
        mProgressTxt = (TextView) view.findViewById(R.id.txt_progress);
    }

    private void setTypeFace()
    {
        error_msg.setTypeface(mContext.getLightTypeFace());
    }

    private void setupForDB()
    {
        modelDownload = new MyDownloadsList();

        mDbHelper = new DatabaseHelper(mContext, "LakshyCareer.sqlite", null,version_val);
        dbAdapters = DatabaseHelper.getDBAdapterInstance(mContext);

        try
        {
            dbAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void getWishlistRequest()
    {
        MasterActivity.mWishlist.clear();

        String userId = SettingsPreferences.getConsumer(mContext).getIdUser();

        try{
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendWishlistRequest(userId, new Callback<WishlistData>() {
                @Override
                public void success(WishlistData model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("WISHLIST_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                mWishlist = model.getMyWishlistData();

                                MasterActivity.mWishlist = mWishlist;
                            }else{
                                msg = model.getMessage();
                            }

                            setListData();


                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        if(mWishlist.size() > 0)
        {
            error_layout_topic.setVisibility(View.GONE);
            list_wishlist.setVisibility(View.VISIBLE);

            WishListAdapter mWishListAdapter= new WishListAdapter(mContext, mWishlist);
            list_wishlist.setAdapter(mWishListAdapter);
        }
        else
        {
            error_layout_topic.setVisibility(View.VISIBLE);
            list_wishlist.setVisibility(View.GONE);
            error_msg.setText(msg);
        }
    }

    public class WishListAdapter extends BaseAdapter
    {
        ArrayList<WishlistItems> mWishlist = new ArrayList<>();

        NavigationDrawerActivity mContext;

        WishListHolder holder;


//        WishlistItems mWishlistItems;

        public WishListAdapter(NavigationDrawerActivity context, ArrayList<WishlistItems> Wishlist)
        {
            mContext = context;
            mWishlist = Wishlist;
        }

        @Override
        public int getCount() {
            return mWishlist.size();
        }

        @Override
        public Object getItem(int position) {
            return mWishlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            holder = null;
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(mContext);
                convertView = layoutInflater.inflate(R.layout.row_wishlist, null);
                holder = new WishListHolder();

                holder.txt_number = (TextView) convertView.findViewById(R.id.txt_number_wish_list);
                holder.txt_subject_name = (TextView) convertView.findViewById(R.id.txt_subject_name_wish_list);
                holder.txt_topic_name = (TextView) convertView.findViewById(R.id.txt_topic_name_wish_list);
                holder.txt_duration = (TextView) convertView.findViewById(R.id.txt_duration);

                holder.img_save = (ImageView) convertView.findViewById(R.id.img_save_wish_list);
                holder.img_favourite = (ImageView) convertView.findViewById(R.id.img_favourite_wish_list);
                holder.img_play = (ImageView) convertView.findViewById(R.id.img_play_wish_list);

                holder.lay_save_wish_list = (LinearLayout) convertView.findViewById(R.id.lay_save_wish_list);
                holder.lay_favourite_wish_list = (LinearLayout) convertView.findViewById(R.id.lay_favourite_wish_list);
                holder.lay_play_wish_list = (LinearLayout) convertView.findViewById(R.id.lay_play_wish_list);

                convertView.setTag(holder);
            } else {

                holder = (WishListHolder) convertView.getTag();
            }

//            mWishlistItems = mWishlist.get(i);

            holder.txt_number.setTypeface(mContext.getBoldTypeFace());
            holder.txt_subject_name.setTypeface(mContext.getBoldTypeFace());
            holder.txt_topic_name.setTypeface(mContext.getRegularTypeFace());
            holder.txt_duration.setTypeface(mContext.getRegularTypeFace());

            int pos = i + 1;
            holder.txt_number.setText(String.valueOf(pos));
            holder.txt_subject_name.setText(mWishlist.get(i).getSubjectName());
            holder.txt_topic_name.setText(mWishlist.get(i).getTopicName());
            holder.txt_duration.setText("Duration - "+mWishlist.get(i).getDuration());

            if(mWishlist.get(i).getFevorite().equalsIgnoreCase("1"))
            {
                holder.img_favourite.setImageResource(R.drawable.ic_favourite_active);
            }
            else
            {
                holder.img_favourite.setImageResource(R.drawable.ic_favourite);
            }

            if(mWishlist.get(i).getIsDownloaded().equalsIgnoreCase("1"))
            {
                holder.img_save.setImageResource(R.drawable.ic_saved_active);
            }
            else
            {
                holder.img_save.setImageResource(R.drawable.ic_save);
            }

            holder.lay_save_wish_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mWishlist.get(i).getIsDownloaded().equalsIgnoreCase("1"))
                    {
                        Toast.makeText(mContext, "You have already downloaded this video!!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        DestinationPath = mContext.CreateFileName(mWishlist.get(i).getTopicName()+"_"+mWishlist.get(i).getIdTopic()+mWishlist.get(i).getVideo());
//                        NAME = mWishlistItems.getTopicName()+mWishlistItems.getVideo();
                        TOPIC_NAME = mWishlist.get(i).getTopicName();
                        TOPIC_ID = mWishlist.get(i).getIdTopic();
                        TOPIC_DURATION = mWishlist.get(i).getDuration();

                        Log.e("TOPIC_ID", ""+TOPIC_ID+"ddd    :"+mWishlist.get(i).getIdTopic());

                        MasterActivity.subjectName = mWishlist.get(i).getSubjectName();

                        initForDownload(mWishlist.get(i).getUrl());

                        if (isInternet) {

                            if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
                                downloadId = downloadManager.add(downloadRequest);
                            }

                            build.setContentText("Download Started");
                            // Removes the progress bar
                            build.setProgress(0, 0, false);
                            mNotifyManager.notify(id, build.build());

                        } else {
                            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            holder.lay_favourite_wish_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    IdTopicWishlist = mWishlist.get(i).getIdTopicWishlist();

                    switch (Integer.parseInt(mWishlist.get(i).getFevorite()))
                    {
                        case 0:
                            addFavoriteRequest();
                            break;
                        case 1:
                            removeFavoriteRequest();
                            break;
                    }
                }
            });

            holder.lay_play_wish_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DestinationPath = mContext.CreateFileName(mWishlist.get(i).getTopicName()+"_"+mWishlist.get(i).getIdTopic()+mWishlist.get(i).getVideo());
                    TOPIC_URL = mWishlist.get(i).getUrl();
                    TOPIC_ID = mWishlist.get(i).getIdTopic();

                    Log.e("DESTINATION_PATH",DestinationPath);


                    if(mWishlist.get(i).getIsDownloaded().equals("1"))
                    {
                        Log.e("CALLL IFFF","");
                        findVideoPathFromLocal();
                    }
                    else
                    {
                        if(isInternet)
                        {
                            topicPlayRequest();
                        }

                        Log.e("TOPIC_URL","TOPIC_URL   :"+TOPIC_URL);

                        Intent videoIntent = new Intent(mContext, VideoPlayerActivity.class);
                        videoIntent.putExtra("video_path",TOPIC_URL);
                        startActivity(videoIntent);
                    }
                }
            });

            return convertView;
        }

        class WishListHolder {

            TextView txt_number, txt_subject_name, txt_topic_name, txt_duration;
            ImageView img_favourite, img_save, img_play;
            LinearLayout lay_save_wish_list, lay_favourite_wish_list, lay_play_wish_list;
        }

        private void topicPlayRequest()
        {
            String userId = SettingsPreferences.getConsumer(mContext).getIdUser();

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.topicPlayRequest(String.valueOf(userId),TOPIC_ID,new Callback<TopicPlay>() {
                @Override
                public void success(TopicPlay content, Response response) {

                    Log.e("TOPIC_PLAY", "" + new Gson().toJson(content));

                    if (response.getStatus() == 200) {
                        try {
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }

        private void addFavoriteRequest()
        {
            String userId = SettingsPreferences.getConsumer(mContext).getIdUser();

            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.AddWishListRequest(String.valueOf(userId),TOPIC_ID,new Callback<WishList>() {
                @Override
                public void success(WishList content, Response response) {

                    mContext.showWaitIndicator(false);

                    Log.e("FAVORITE", "" + new Gson().toJson(content));

                    if (response.getStatus() == 200) {
                        try {
                            Toast.makeText(mContext,content.getMessage(),Toast.LENGTH_LONG).show();
                            getWishlistRequest();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }

        private void removeFavoriteRequest()
        {
            mContext.showWaitIndicator(true);

            Log.e("getIdTopicWishlist",IdTopicWishlist);
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.RemoveWishListRequest(IdTopicWishlist,new Callback<RemoveWishList>() {
                @Override
                public void success(RemoveWishList content, Response response) {

                    mContext.showWaitIndicator(false);

                    Log.e("UNFAVORITE", "" + new Gson().toJson(content));

                    if (response.getStatus() == 200) {
                        try {
                            Toast.makeText(mContext,content.getMessage(),Toast.LENGTH_LONG).show();
                            getWishlistRequest();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
    }

    private void initForDownload(String DownloadURL)
    {
        APP_PATH_SD_CARD = APP_PATH_SD_CARD+"/Topics"+"/" ;

        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();

//        File filesDir = getExternalFilesDir("");
        File filesDir = mContext.getExternalFilesDir(APP_PATH_SD_CARD);

        Uri downloadUri = Uri.parse(DownloadURL);
        destinationUri = Uri.parse(filesDir+"/"+DestinationPath);
        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download")
                .setStatusListener(myDownloadStatusListener);


        mNotifyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        build = new NotificationCompat.Builder(mContext);
        build.setContentTitle(""+TOPIC_NAME)
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_file_download_white_18dp);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0,
                new Intent(mContext, MyDownloadsActivity.class),
                0);

        build.setContentIntent(resultPendingIntent);
        build.setAutoCancel(true);
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {

        @Override
        public void onDownloadComplete(DownloadRequest request) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
                mProgressTxt.setText(request.getDownloadContext() + "Download Completed");

                build.setContentText("Download Completed");
                // Removes the progress bar
                build.setProgress(0, 0, false);
                mNotifyManager.notify(id, build.build());

                addDownloadRequest();


            }
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
                mProgressTxt.setText("Download Failed");
                mProgress.setProgress(0);

                build.setContentText("Download Failed");
                mNotifyManager.notify(id, build.build());
            }
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();

            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
            if (id == downloadId) {
                mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                mProgress.setProgress(progress);

                build.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                build.setProgress(100, progress, false);
                mNotifyManager.notify(id, build.build());

            }
        }
    }

    private String getBytesDownloaded(int progress, long totalBytes) {
        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }
    }

    private void addDownloadRequest()
    {
        String userId = SettingsPreferences.getConsumer(mContext).getIdUser();

        mContext.showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.addDownloadRequest(userId,TOPIC_ID,new Callback<AddDownload>() {
            @Override
            public void success(AddDownload content, Response response) {

                mContext.showWaitIndicator(false);

                Log.e("Add_DOWNLOAD", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                        Toast.makeText(mContext,content.getMessage(),Toast.LENGTH_LONG).show();

                        build.setContentText("Download Completed");
                        // Removes the progress bar
                        build.setProgress(0, 0, false);
                        mNotifyManager.notify(id, build.build());

                        Log.e("Add_DOWNLOAD", "" + content.getIdMyDownload());

                        try
                        {
                            dbAdapters.openDataBase();

                            modelDownload.setVideo_duration(TOPIC_DURATION);
                            modelDownload.setVideo_path(destinationUri.toString());
                            modelDownload.setVideo_subject_name(MasterActivity.subjectName);
                            modelDownload.setVideo_topic_name(TOPIC_NAME);
                            modelDownload.setVideo_topic_download_id(String.valueOf(content.getIdMyDownload()));

                            dbAdapters.Insert_Record(modelDownload);

                            dbAdapters.close();

                            getWishlistRequest();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                mContext.showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

//                    new WishlistFragment()
                    getActivity().finish();
                    Log.e("Tag", "Wish OnBack");
                    mContext.startActivity(new Intent(mContext,NavigationDrawerActivity.class));
                    return true;
                }
                return false;
            }
        });
    }

    private void findVideoPathFromLocal()
    {
        try {
            String path = null;
            dbAdapters.openDataBase();
            mMyDownloadlist = dbAdapters.Select_Record();
            dbAdapters.close();

            String searchString = DestinationPath;
            Log.e("searchString",searchString);

            for(int i = 0; i < mMyDownloadlist.size(); i++)
            {
                if(mMyDownloadlist.get(i).getVideo_path().contains(searchString))
                {
                    path = mMyDownloadlist.get(i).getVideo_path();
                    Log.e("DOWNLOADED_PATH",path);
                    break;
                }
            }

//                if(path == null){
//                    path = TOPIC_URL;
//                }

            Intent videoIntent = new Intent(mContext, VideoPlayerActivity.class);
            videoIntent.putExtra("video_path",path);
            startActivity(videoIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
