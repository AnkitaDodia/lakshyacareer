package com.lakshyacareer.digitalcoaching;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lakshyacareer.digitalcoaching.adapter.CategoryAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Category;
import com.lakshyacareer.digitalcoaching.model.CategoryData;
import com.lakshyacareer.digitalcoaching.model.MaterialsDownloads;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.DBMaterialsHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadManager;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 10/31/2017.
 */

public class MaterialCategoriesActivity extends MasterActivity
{
    Context ctx;
    private boolean isInternet;

    ListView list_topic;

    private ArrayList<CategoryData> mCategoryList = new ArrayList<>();

    TextView txt_action_title_topic,error_msg;

    LinearLayout layout_back_topic,error_layout_topic;

    String msg = "No Material Available";

    public String APP_PATH_SD_CARD = "/Materials/";
    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();
    Uri destinationUri;
    int id = 1, mPos = 0;
    String ChannelId = "my_channel_01";

    DownloadRequest downloadRequest;
    int downloadId;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mNotificationBuilder;

    //    //Code for database
    DBMaterialsHelper mDbHelper, dbAdapters;
	static int version_val = 2;
    MaterialsDownloads mMaterialsDownloads;

    //Coed for palying from local if video is already downloaded
    private ArrayList<MaterialsDownloads> mMaterialsDownloadsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_topic_list);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(this);

        inItView();
        setTypeFace();
        setClickListener();
        setupForDB();

        if(MasterActivity.CategoryList.size() > 0)
        {
            mCategoryList = MasterActivity.CategoryList;
            setListData();
        }
        else
        {
            if (isInternet) {
                getCategotyRequest();
            } else {
                Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void inItView() {
        txt_action_title_topic = (TextView) findViewById(R.id.txt_action_title);
        error_msg = (TextView) findViewById(R.id.error_msg);

        layout_back_topic = (LinearLayout) findViewById(R.id.layout_back);
        error_layout_topic = (LinearLayout) findViewById(R.id.error_layout_topic);

        list_topic = (ListView) findViewById(R.id.list_topic);

        txt_action_title_topic.setText(MasterActivity.CategoryName);
    }

    private void setTypeFace()
    {
        txt_action_title_topic.setTypeface(getBoldTypeFace());
        error_msg.setTypeface(getLightTypeFace());
    }

    private void setClickListener()
    {
        layout_back_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        list_topic.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3)
            {
                String MaterialName = mCategoryList.get(position).getMaterial();

                mPos = position;
                initForDownload(position, MaterialName);

                if (isInternet) {

                    if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
                        downloadId = downloadManager.add(downloadRequest);
                    }

                    mNotificationBuilder.setContentText("Download Started");
                    // Removes the progress bar
                    mNotificationBuilder.setProgress(0, 0, false);
                    mNotifyManager.notify(id, mNotificationBuilder.build());

                } else {
                    Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void getCategotyRequest()
    {
        try{
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.getCategotyRequest(CategoryId,new Callback<Category>() {
                @Override
                public void success(Category model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CATEGOTY_LIST_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                mCategoryList = model.getData();

                                MasterActivity.CategoryList = mCategoryList;
                            }

//                            msg = model.getMessage();

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void setListData()
    {
        if(mCategoryList.size() > 0)
        {
            error_layout_topic.setVisibility(View.GONE);

            CategoryAdapter mCategoryAdapter = new CategoryAdapter(this, mCategoryList);
            list_topic.setAdapter(mCategoryAdapter);
        }
        else
        {
            error_layout_topic.setVisibility(View.VISIBLE);
            list_topic.setVisibility(View.GONE);
            error_msg.setText(msg);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }

    private void initForDownload(int pos, String DownloadfileName)
    {
        String DownloadURL = mCategoryList.get(pos).getMaterialUrl();
        APP_PATH_SD_CARD = APP_PATH_SD_CARD+"/Category"+"/" ;

        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();

//        File filesDir = getExternalFilesDir("");
        File filesDir = getExternalFilesDir(APP_PATH_SD_CARD);

        String DestinationPath = filesDir+"/"+CreateFileName(DownloadfileName);


        Log.e("DestinationPath","DestinationPath    :"+DestinationPath);


        Uri downloadUri = Uri.parse(DownloadURL);
        destinationUri = Uri.parse(DestinationPath);
        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download")
                .setStatusListener(myDownloadStatusListener);


        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel = new NotificationChannel(ChannelId, "channel_name",NotificationManager.IMPORTANCE_LOW);
            mChannel.setSound(null, null);
            mNotifyManager.createNotificationChannel(mChannel);
        }
//        build = new NotificationCompat.Builder(ctx);
        mNotificationBuilder = new NotificationCompat.Builder(ctx, ChannelId);
        mNotificationBuilder.setContentTitle(""+DownloadfileName)
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_file_download_white_18dp);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, DownloadsTabsActivity.class),
                0);

        mNotificationBuilder.setContentIntent(resultPendingIntent);
        mNotificationBuilder.setAutoCancel(true);
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {

        @Override
        public void onDownloadComplete(DownloadRequest request) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText(request.getDownloadContext() + "Download Completed");

                mNotificationBuilder.setContentText("Download Completed");
                // Removes the progress bar
                mNotificationBuilder.setProgress(100, 100, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());


                try
                {
                    dbAdapters.openDataBase();

                    Log.e("destinationUri",destinationUri.toString());

                    mMaterialsDownloads.setMaterialId(mCategoryList.get(mPos).getIdMaterial());
                    mMaterialsDownloads.setMaterialName(mCategoryList.get(mPos).getTitle());
                    mMaterialsDownloads.setMaterialCategoryName(mCategoryList.get(mPos).getCategory());
                    mMaterialsDownloads.setMaterialPath(destinationUri.toString());
                    mMaterialsDownloads.setMaterialSize(mCategoryList.get(mPos).getExtension());

                    dbAdapters.InsertMaterials(mMaterialsDownloads);

                    dbAdapters.close();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }



                Toast.makeText(ctx, "Download Completed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
//                mProgressTxt.setText("Download Failed");
//                mProgress.setProgress(0);

                mNotificationBuilder.setContentText("Download Failed");
                mNotifyManager.notify(id, mNotificationBuilder.build());
            }
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();

            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
            if (id == downloadId) {
//                mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
//                mProgress.setProgress(progress);

                mNotificationBuilder.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                mNotificationBuilder.setProgress(100, progress, false);
                mNotifyManager.notify(id, mNotificationBuilder.build());

            }
        }
    }

    private String getBytesDownloaded(int progress, long totalBytes) {
        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }
    }

    private void setupForDB()
    {
        mMaterialsDownloads = new MaterialsDownloads();

        mDbHelper = new DBMaterialsHelper(this, "LakshyCareer.sqlite", null,version_val);
        dbAdapters = DBMaterialsHelper.getDBAdapterInstance(this);

        try
        {
            dbAdapters.createDataBase();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
