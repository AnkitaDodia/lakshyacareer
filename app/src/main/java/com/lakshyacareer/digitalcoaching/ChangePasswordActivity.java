package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.ChangePassword;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 02-11-2017.
 */

public class ChangePasswordActivity extends MasterActivity {

    Context ctx;

    LinearLayout layout_back;
    TextView txt_action_title;

    TextInputLayout input_current_pass, input_new_password, input_confirm_new_pass;
    EditText edit_current_pass, edit_new_pass,edit_confirm_new_pass;

    Button btn_change_password;

    private boolean isInternet;

    String id_user;
    String password, new_password, confirm_new_password ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();


    }

    private void getView()
    {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_action_title.setText("Change Password");

        btn_change_password = (Button)findViewById(R.id.btn_change_password);

        input_current_pass = (TextInputLayout) findViewById(R.id.input_current_pass);
        input_new_password = (TextInputLayout) findViewById(R.id.input_new_password);
        input_confirm_new_pass = (TextInputLayout) findViewById(R.id.input_confirm_new_pass);

        edit_current_pass = (EditText) findViewById(R.id.edit_current_pass);
        edit_new_pass = (EditText) findViewById(R.id.edit_new_pass);
        edit_confirm_new_pass = (EditText) findViewById(R.id.edit_confirm_new_pass);

    }

    private void setTypeFace()
    {
        txt_action_title.setTypeface(getBoldTypeFace());

        btn_change_password.setTypeface(getBoldTypeFace());

        input_current_pass.setTypeface(getRegularTypeFace());
        input_new_password.setTypeface(getRegularTypeFace());
        input_confirm_new_pass.setTypeface(getRegularTypeFace());

        edit_current_pass.setTypeface(getRegularTypeFace());
        edit_new_pass.setTypeface(getRegularTypeFace());
        edit_confirm_new_pass.setTypeface(getRegularTypeFace());


    }

    private void setClickListener() {

        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                password = edit_current_pass.getText().toString();
                new_password = edit_new_pass.getText().toString();
                confirm_new_password = edit_confirm_new_pass.getText().toString();

                if (edit_current_pass.getText().toString().length() == 0) {
                    edit_current_pass.setError("Please enter password");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }
//                else if (!getLoginPwd().equals(password)) {
//                    edit_current_pass.setError("Enter Correct Password");
////                    Toast.makeText(ctx,"P" +getLoginPwd(),Toast.LENGTH_LONG).show();
//                }

                else if (edit_new_pass.getText().toString().length() == 0) {
                    edit_new_pass.setError("Please enter password");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                }else if (!isAlphaNumeric(new_password)) {
                    edit_new_pass.setError("Please enter at alphanumeric password");
//                    Toast.makeText(ctx,"Please enter at alphanumeric password",Toast.LENGTH_LONG).show();
                } else if (new_password.length() < 6) {
                    edit_new_pass.setError("Please enter at least six character password");
//                    Toast.makeText(ctx,"Please enter at least six character password",Toast.LENGTH_LONG).show();
                }

//                else if (edit_confirm_new_pass.getText().toString().length() == 0) {
//                    edit_confirm_new_pass.setError("Your password dose't match");
////                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
//                }
                else if (!confirm_new_password.equalsIgnoreCase(new_password)) {
                    edit_confirm_new_pass.setError("Your password dose't match");
//                    Toast.makeText(ctx,"Please enter at alphanumeric password",Toast.LENGTH_LONG).show();
                }
                else {
                    if (isInternet) {
                        sendChangePasswordRequest();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void sendChangePasswordRequest() {
        try {

            id_user = SettingsPreferences.getConsumer(this).getIdUser();

//            String password = edit_current_pass.getText().toString();
//            String new_password = edit_new_pass.getText().toString();
//            String confirm_new_password = edit_confirm_new_pass.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendChangePasswordRequest(id_user, password, new_password, confirm_new_password
                    , new Callback<ChangePassword>() {
                        @Override
                        public void success(ChangePassword model, Response response) {

                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("REGISTER_RESPONSE", "" + new Gson().toJson(model));

                                    if (model.getStatus() == 1) {

                                        saveLoginPwd(new_password);

                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();
                                        finish();

                                    } else {
                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();
                                    }


                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showWaitIndicator(false);

                            Toast.makeText(ctx, "failure", Toast.LENGTH_LONG).show();

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}