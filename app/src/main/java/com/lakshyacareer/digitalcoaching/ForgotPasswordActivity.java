package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.ForgotPassword;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 10/27/2017.
 */
public class ForgotPasswordActivity extends MasterActivity
{
    Context ctx;

    private boolean isInternet;

    LinearLayout layout_back_forgot;

    TextView txt_action_title_forgot, txt_forgotten_msg;

    TextInputLayout input_layout_email_forgot;

    EditText edt_email_forgot;

    Button btn_send;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();
    }

    private void getView() {
        layout_back_forgot = (LinearLayout) findViewById(R.id.layout_back);

        txt_action_title_forgot = (TextView) findViewById(R.id.txt_action_title);
        txt_forgotten_msg = (TextView) findViewById(R.id.txt_forgotten_msg);

        input_layout_email_forgot = (TextInputLayout) findViewById(R.id.input_layout_email_forgot);

        edt_email_forgot = (EditText) findViewById(R.id.edt_email_forgot);

        btn_send = (Button) findViewById(R.id.btn_send);

        txt_action_title_forgot.setText("Forgot Password");
    }

    private void setTypeFace() {
        txt_action_title_forgot.setTypeface(getBoldTypeFace());
        txt_forgotten_msg.setTypeface(getRegularTypeFace());
        btn_send.setTypeface(getBoldTypeFace());

        input_layout_email_forgot.setTypeface(getRegularTypeFace());
        edt_email_forgot.setTypeface(getRegularTypeFace());
    }

    private void setClickListener()
    {
        layout_back_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt_email_forgot.getText().toString().length() == 0)
                {
                    edt_email_forgot.setError("Please enter email");
//                    Toast.makeText(ctx,"Please enter email address",Toast.LENGTH_LONG).show();
                }
                else if (!isValidEmail(edt_email_forgot.getText().toString())) {
                    edt_email_forgot.setError("Please enter valid email");
//                    Toast.makeText(ctx,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendForgotPasswordRequest()
    {
        try {
            String email = edt_email_forgot.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendForgotPasswordRequest(email,new Callback<ForgotPassword>() {
                @Override
                public void success(ForgotPassword model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("FORGOT_RESPONSE", "" + new Gson().toJson(model));

                            Toast.makeText(ctx,model.getMessage(),Toast.LENGTH_LONG).show();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
