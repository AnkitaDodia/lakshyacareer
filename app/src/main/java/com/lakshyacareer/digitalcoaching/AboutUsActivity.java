package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.AboutUs;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 31-10-2017.
 */

public class AboutUsActivity extends MasterActivity {

    Context ctx;
    TextView txt_action_title, txt_about_content;
    LinearLayout layout_back;

    boolean isInternet;

    WebView webview_about;
    String text = null;
    CardView card_aboutus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        initViews();
        setTypeFace();
        setClickListener();


        if(MasterActivity.mText != null)
        {
            text = MasterActivity.mText;

            webview_about.loadData("<font>" + text + "</font>",
                    "text/html; charset=UTF-8", null);
        }else{
            if (isInternet) {
                sendContentRequest();
            }
            else {
                Toast.makeText(ctx, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initViews() {
        layout_back = (LinearLayout) findViewById(R.id.layout_back);

        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_about_content = (TextView)findViewById(R.id.txt_about_content);

        webview_about = (WebView) findViewById(R.id.webview_about);

        card_aboutus = (CardView)findViewById(R.id.card_aboutus);
        card_aboutus.setVisibility(View.GONE);
    }

    private void setTypeFace()
    {
        txt_action_title.setTypeface(getBoldTypeFace());
        txt_about_content.setTypeface(getRegularTypeFace());

        txt_action_title.setText("About Us");
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void sendContentRequest(){
        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.getAboutUs(new Callback<AboutUs>() {
            @Override
            public void success(AboutUs content, Response response) {

                if (response.getStatus() == 200) {
                    try {
                        String about = content.getMessage();
                        String text = "<html><body style=\"color:#484848;font-family:file:///android_asset//ProximaNovaSemibold.ttf;font-size:20px;\"'background-color:transparent' >" + "<p align=\"justify\">" + about + "</p>" + "</body></html>";

                        MasterActivity.mText = text;

                        webview_about.loadData("<font>" + text + "</font>",
                                "text/html; charset=UTF-8", null);

//                        Toast.makeText(ctx, "Tessstttt", Toast.LENGTH_LONG).show();
                        card_aboutus.setVisibility(View.VISIBLE);
                        showWaitIndicator(false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);

                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}