package com.lakshyacareer.digitalcoaching;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.lakshyacareer.digitalcoaching.adapter.TopicListAdapter;
import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.TopicList;
import com.lakshyacareer.digitalcoaching.model.TopicListData;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.google.gson.Gson;
import java.util.ArrayList;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by My 7 on 10/31/2017.
 */

public class TopicListActivity extends MasterActivity
{
    private boolean isInternet;

    ListView list_topic;

    private ArrayList<TopicListData> topicList = new ArrayList<>();

    TextView txt_action_title_topic,error_msg;

    LinearLayout layout_back_topic,error_layout_topic;

    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_topic_list);

        isInternet = new InternetStatus().isInternetOn(this);

        inItView();
        setTypeFace();
        setClickListener();

        if(MasterActivity.topicList.size() > 0)
        {
            topicList = MasterActivity.topicList;
            setListData();
        }
        else
        {
            if (isInternet) {
                getTopicListRequest();
            } else {
                Toast.makeText(this, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void inItView() {
        txt_action_title_topic = (TextView) findViewById(R.id.txt_action_title);
        error_msg = (TextView) findViewById(R.id.error_msg);

        layout_back_topic = (LinearLayout) findViewById(R.id.layout_back);
        error_layout_topic = (LinearLayout) findViewById(R.id.error_layout_topic);

        list_topic = (ListView) findViewById(R.id.list_topic);

        txt_action_title_topic.setText(MasterActivity.subjectName);
    }

    private void setTypeFace()
    {
        txt_action_title_topic.setTypeface(getBoldTypeFace());
        error_msg.setTypeface(getLightTypeFace());
    }

    private void setClickListener()
    {
        layout_back_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getTopicListRequest()
    {
        try{
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendTopicRequest(subjectId,new Callback<TopicList>() {
                @Override
                public void success(TopicList model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("TOPIC_LIST_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus() == 1)
                            {
                                topicList = model.getTopicListData();

                                MasterActivity.topicList = topicList;
                            }

                            msg = model.getMessage();

                            setListData();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    showWaitIndicator(false);

                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void setListData()
    {
        if(topicList.size() > 0)
        {
            error_layout_topic.setVisibility(View.GONE);

            TopicListAdapter mTopicListAdapter = new TopicListAdapter(this, topicList);
            list_topic.setAdapter(mTopicListAdapter);
        }
        else
        {
            error_layout_topic.setVisibility(View.VISIBLE);
            list_topic.setVisibility(View.GONE);
            error_msg.setText(msg);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
