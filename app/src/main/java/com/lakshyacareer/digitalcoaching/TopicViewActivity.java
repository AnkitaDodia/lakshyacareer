package com.lakshyacareer.digitalcoaching;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.AddDownload;
import com.lakshyacareer.digitalcoaching.model.MyDownloadsList;
import com.lakshyacareer.digitalcoaching.model.RemoveWishList;
import com.lakshyacareer.digitalcoaching.model.TopicPlay;
import com.lakshyacareer.digitalcoaching.model.TopicView;
import com.lakshyacareer.digitalcoaching.model.TopicViewData;
import com.lakshyacareer.digitalcoaching.model.WishList;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
//import com.lakshyacareer.digitalcoaching.service.DownloadService;
import com.lakshyacareer.digitalcoaching.utility.DatabaseHelper;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadManager;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;


import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by abc on 07-11-2017.
 */

public class TopicViewActivity extends MasterActivity {

    Context ctx;

    TextView txt_action_title, txt_position, txt_topic_name, txt_subject_name, txt_duration, txt_size;

    public static TextView mProgressTxt;

    LinearLayout layout_back, lay_save, lay_favorite, lay_play;

    ImageView img_favorite_topic_view,img_save_topic_view;

    CardView card_view;

    private boolean isInternet;

    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;

    MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();

    DownloadRequest downloadRequest;
    int downloadId;

//    String DownloadURL;

//    ProgressBar mProgress;

    public String APP_PATH_SD_CARD = "/Subjects/";

    private NotificationManager mNotifyManager;
    private Builder build;
    int id = 1;

    public static ProgressBar mProgress;

    TopicViewData mTopic;

    //Code for database
    DatabaseHelper mDbHelper, dbAdapters;
	SQLiteDatabase mDb;
	static int version_val = 1;
    MyDownloadsList modelDownload;
    Uri destinationUri;

    String ChannelId = "my_channel_02";

    //Coed for palying from local if video is already downloaded
    private ArrayList<MyDownloadsList> mMyDownloadlist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_view);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

//        DownloadURL = "http://admin.lakshyacareer.in/uploads/AuDagYWrSh.mp4";
//        Log.e("DownloadURL",""+DownloadURL);

        inItView();
        setTypeFace();
        setClickListener();
        setupForDB();

        if (isInternet) {
            topicViewRequest();
        }
        else {
            Toast.makeText(ctx, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    private void setupForDB()
    {
        modelDownload = new MyDownloadsList();

        mDbHelper = new DatabaseHelper(this, "LakshyCareer.sqlite", null,version_val);
		dbAdapters = DatabaseHelper.getDBAdapterInstance(this);

		try
		{
			dbAdapters.createDataBase();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
    }

    private void inItView() {
        card_view = (CardView) findViewById(R.id.card_view);

        txt_position = (TextView) findViewById(R.id.txt_position);
        txt_topic_name = (TextView) findViewById(R.id.txt_topic_name);
        txt_subject_name = (TextView) findViewById(R.id.txt_subject_name);
        txt_duration = (TextView) findViewById(R.id.txt_duration);
        txt_size = (TextView) findViewById(R.id.txt_size);

        txt_action_title = (TextView) findViewById(R.id.txt_action_title);

        img_favorite_topic_view = (ImageView) findViewById(R.id.img_favorite_topic_view);
        img_save_topic_view = (ImageView) findViewById(R.id.img_save_topic_view);

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        lay_save = (LinearLayout) findViewById(R.id.lay_save);
        lay_favorite = (LinearLayout) findViewById(R.id.lay_favorite);
        lay_play = (LinearLayout) findViewById(R.id.lay_play);

        txt_action_title.setText(MasterActivity.subjectName);

        mProgress = (ProgressBar)findViewById(R.id.prgbr_download);
        mProgressTxt = (TextView) findViewById(R.id.txt_progress);
    }

    private void setTypeFace()
    {
        txt_action_title.setTypeface(getBoldTypeFace());
        txt_position.setTypeface(getBoldTypeFace());

        txt_topic_name.setTypeface(getBoldTypeFace());
        txt_subject_name.setTypeface(getBoldTypeFace());
        txt_duration.setTypeface(getRegularTypeFace());
        txt_size.setTypeface(getRegularTypeFace());
    }

    private void setData()
    {
        txt_topic_name.setText(mTopic.getTopicName());
        txt_subject_name.setText(MasterActivity.subjectName);
        txt_duration.setText("Duration - "+mTopic.getDuration());
        txt_size.setText("Size - "+mTopic.getSize());

        if(mTopic.getFevorite().equalsIgnoreCase("1"))
        {
            img_favorite_topic_view.setImageResource(R.drawable.ic_favourite_active);
        }
        else
        {
            img_favorite_topic_view.setImageResource(R.drawable.ic_favourite);
        }

        if(mTopic.getIsDownloaded() == 1)
        {
            img_save_topic_view.setImageResource(R.drawable.ic_saved_active);
        }
        else
        {
            img_save_topic_view.setImageResource(R.drawable.ic_save);
        }

        card_view.setVisibility(View.VISIBLE);
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        lay_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mTopic.getIsDownloaded() == 1)
                {
                    Toast.makeText(ctx, "You have already downloaded this video!!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    /*Intent mServiceIntent = new Intent(ctx, DownloadService.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("NAME",mTopic.getTopicName());
                    bundle.putString("ID",mTopic.getIdTopic());
                    bundle.putString("VIDEO",mTopic.getVideo());
                    bundle.putString("URL",mTopic.getUrl());

                    mServiceIntent.putExtra("BUNDLE",bundle);
                    startService(mServiceIntent);*/

                    initForDownload(mTopic.getUrl());

                    if (isInternet) {

                        if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
                            downloadId = downloadManager.add(downloadRequest);
                        }

                        build.setContentText("Download Started");
                        // Removes the progress bar
                        build.setProgress(0, 0, false);
                        mNotifyManager.notify(id, build.build());

                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        lay_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MasterActivity.mWishlist.clear();

                if (isInternet)
                {


                    switch (Integer.parseInt(mTopic.getFevorite()))
                    {
                        case 0:
                            addFavoriteRequest();
                            break;
                        case 1:
                            removeFavoriteRequest();
                            break;
                    }
                }
                else
                {
                    Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lay_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mTopic.getIsDownloaded() == 1)
                {
                    findVideoPathFromLocal();
                }
                else
                {
                    if(isInternet)
                    {
                        topicPlayRequest();
                    }

                    Intent videoIntent = new Intent(ctx, VideoPlayerActivity.class);
                    videoIntent.putExtra("video_path",mTopic.getUrl());
                    startActivity(videoIntent);
                }
            }
        });
    }

    private void findVideoPathFromLocal()
    {
        try {
            String path = null;
            dbAdapters.openDataBase();
            mMyDownloadlist = dbAdapters.Select_Record();
            dbAdapters.close();

            if(mMyDownloadlist.size() > 0)
            {
                String searchString = CreateFileName(mTopic.getTopicName()+"_"+mTopic.getIdTopic()+mTopic.getVideo());
                Log.e("searchString",searchString);

                for(int i = 0; i < mMyDownloadlist.size(); i++)
                {
                    if(mMyDownloadlist.get(i).getVideo_path().contains(searchString))
                    {
                        path = mMyDownloadlist.get(i).getVideo_path();
                        Log.e("DOWNLOADED_PATH",path);
                        break;
                    }
                }

                Intent videoIntent = new Intent(ctx, VideoPlayerActivity.class);
                videoIntent.putExtra("video_path",path);
                startActivity(videoIntent);
            }
            else
            {
                Intent videoIntent = new Intent(ctx, VideoPlayerActivity.class);
                videoIntent.putExtra("video_path",mTopic.getUrl());
                startActivity(videoIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initForDownload(String DownloadURL)
    {
        APP_PATH_SD_CARD = APP_PATH_SD_CARD+"/Topics"+"/" ;

        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        RetryPolicy retryPolicy = new DefaultRetryPolicy();

//        File filesDir = getExternalFilesDir("");
        File filesDir = getExternalFilesDir(APP_PATH_SD_CARD);

        String DestinationPath = filesDir+"/"+CreateFileName(mTopic.getTopicName()+"_"+mTopic.getIdTopic()+mTopic.getVideo());


        Log.e("DestinationPath","DestinationPath    :"+DestinationPath);


        Uri downloadUri = Uri.parse(DownloadURL);
        destinationUri = Uri.parse(DestinationPath);
        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.LOW)
                .setRetryPolicy(retryPolicy)
                .setDownloadContext("Download")
                .setStatusListener(myDownloadStatusListener);


        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel mChannel = new NotificationChannel(ChannelId, "channel_name",NotificationManager.IMPORTANCE_LOW);
            mChannel.setSound(null, null);
            mNotifyManager.createNotificationChannel(mChannel);
        }
//        build = new NotificationCompat.Builder(ctx);
        build = new NotificationCompat.Builder(ctx, ChannelId);
        build.setContentTitle(""+mTopic.getTopicName())
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_file_download_white_18dp);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, DownloadsTabsActivity.class),
                0);

        build.setContentIntent(resultPendingIntent);
        build.setAutoCancel(true);
    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {

        @Override
        public void onDownloadComplete(DownloadRequest request) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
                mProgressTxt.setText(request.getDownloadContext() + "Download Completed");

                build.setContentText("Download Completed");
                // Removes the progress bar
                build.setProgress(100, 100, false);
                mNotifyManager.notify(id, build.build());

                addDownloadRequest();
            }
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {
            final int id = request.getDownloadId();
            if (id == downloadId) {
                mProgressTxt.setText("Download Failed");
                mProgress.setProgress(0);

                build.setContentText("Download Failed");
                mNotifyManager.notify(id, build.build());
            }
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();

            System.out.println("######## onProgress ###### "+id+" : "+totalBytes+" : "+downloadedBytes+" : "+progress);
            if (id == downloadId) {
                mProgressTxt.setText("Downloading...  "+", "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                mProgress.setProgress(progress);

                build.setContentText("Downloading... "+progress+"%"+"  "+getBytesDownloaded(progress,totalBytes));
                build.setProgress(100, progress, false);
                mNotifyManager.notify(id, build.build());

            }
        }
    }

    private String getBytesDownloaded(int progress, long totalBytes) {
        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }
    }

    private void topicViewRequest()
    {
        String userId = SettingsPreferences.getConsumer(this).getIdUser();
        Log.e("USER_ID",""+userId);

        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.topicViewRequest(userId,topicId,new Callback<TopicView>() {
            @Override
            public void success(TopicView content, Response response) {

                showWaitIndicator(false);

                Log.e("TOPIC_VIEW", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                        mTopic = content.getTopicViewData();

                        setData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void topicPlayRequest()
    {
        String userId = SettingsPreferences.getConsumer(this).getIdUser();

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.topicPlayRequest(userId,topicId,new Callback<TopicPlay>() {
            @Override
            public void success(TopicPlay content, Response response) {

                Log.e("TOPIC_PLAY", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void addFavoriteRequest()
    {
        String userId = SettingsPreferences.getConsumer(this).getIdUser();

        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.AddWishListRequest(userId,topicId,new Callback<WishList>() {
            @Override
            public void success(WishList content, Response response) {

                showWaitIndicator(false);

                Log.e("ADD_FAVORITE", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                        Toast.makeText(ctx,content.getMessage(),Toast.LENGTH_LONG).show();
                        topicViewRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    private void removeFavoriteRequest()
    {
        showWaitIndicator(true);

        Log.e("getIdTopicWishlist",""+mTopic.getIdTopicWishlist());
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.RemoveWishListRequest(String.valueOf(mTopic.getIdTopicWishlist()),new Callback<RemoveWishList>() {
            @Override
            public void success(RemoveWishList content, Response response) {

                showWaitIndicator(false);

                Log.e("UNFAVORITE", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                        Toast.makeText(ctx,content.getMessage(),Toast.LENGTH_LONG).show();
                        topicViewRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    public void addDownloadRequest()
    {
        String userId = SettingsPreferences.getConsumer(this).getIdUser();

        showWaitIndicator(true);

        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

        RestInterface restInterface = adapter.create(RestInterface.class);

        restInterface.addDownloadRequest(userId,topicId,new Callback<AddDownload>() {
            @Override
            public void success(AddDownload content, Response response) {

                showWaitIndicator(false);

                Log.e("Add_DOWNLOAD", "" + new Gson().toJson(content));

                if (response.getStatus() == 200) {
                    try {
                        Toast.makeText(ctx,content.getMessage(),Toast.LENGTH_LONG).show();

                        build.setContentText("Download Completed");
                        // Removes the progress bar
                        build.setProgress(0, 0, false);
                        mNotifyManager.notify(id, build.build());

                        try
                        {
                            dbAdapters.openDataBase();

                            Log.e("destinationUri",destinationUri.toString());

                            modelDownload.setVideo_duration(mTopic.getDuration());
                            modelDownload.setVideo_path(destinationUri.toString());
                            modelDownload.setVideo_subject_name(MasterActivity.subjectName);
                            modelDownload.setVideo_topic_name(mTopic.getTopicName());
                            modelDownload.setVideo_topic_download_id(String.valueOf(content.getIdMyDownload()));

                            dbAdapters.Insert_Record(modelDownload);

                            dbAdapters.close();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                        topicViewRequest();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

                showWaitIndicator(false);
                Log.e("ERROR", "" + error.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(MasterActivity.fromTopicView)
        {
            Toast.makeText(TopicViewActivity.this,"Call",Toast.LENGTH_LONG).show();
            addDownloadRequest();
            MasterActivity.fromTopicView = false;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}