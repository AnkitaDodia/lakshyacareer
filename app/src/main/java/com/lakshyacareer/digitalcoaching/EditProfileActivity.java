package com.lakshyacareer.digitalcoaching;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshyacareer.digitalcoaching.common.MasterActivity;
import com.lakshyacareer.digitalcoaching.model.Data;
import com.lakshyacareer.digitalcoaching.model.User;
import com.lakshyacareer.digitalcoaching.restinterface.RestInterface;
import com.lakshyacareer.digitalcoaching.utility.InternetStatus;
import com.lakshyacareer.digitalcoaching.utility.SettingsPreferences;
import com.google.gson.Gson;

import java.util.Arrays;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by abc on 02-11-2017.
 */

public class EditProfileActivity extends MasterActivity {

    Context ctx;

    LinearLayout layout_back;
    TextView txt_action_title;

    private TextInputLayout input_first_name, input_last_name, input_dob, input_email, input_mobile, input_state, input_city;
    private EditText edit_first_name, edit_last_name, edit_dob, edit_email, edit_mobile, edit_state, edit_city;
    private Button btn_update;

    private boolean isInternet;
    String spinnerValue;
    Spinner spinner_state;
    int selectedItem = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        ctx = this;

        isInternet = new InternetStatus().isInternetOn(ctx);

        getView();
        setTypeFace();
        setClickListener();
        setSpinnerAdapter();
        getDetails();
    }

    private void getView(){

        input_first_name = (TextInputLayout)findViewById( R.id.input_first_name );
        input_last_name = (TextInputLayout)findViewById( R.id.input_last_name );
        input_email = (TextInputLayout)findViewById( R.id.input_email );
        input_mobile = (TextInputLayout)findViewById( R.id.input_mobile );
//        input_state = (TextInputLayout)findViewById( R.id.input_state );
        input_city = (TextInputLayout)findViewById( R.id.input_city );
        input_dob = (TextInputLayout)findViewById( R.id.input_dob );

        edit_first_name = (EditText)findViewById( R.id.edit_first_name );
        edit_last_name = (EditText)findViewById( R.id.edit_last_name );
        edit_email = (EditText)findViewById( R.id.edit_email );
        edit_mobile = (EditText)findViewById( R.id.edit_mobile );
//        edit_state = (EditText)findViewById( R.id.edit_state );
        edit_city = (EditText)findViewById( R.id.edit_city );
        edit_dob = (EditText)findViewById( R.id.edit_dob );

        btn_update = (Button)findViewById( R.id.btn_update);

        layout_back = (LinearLayout) findViewById(R.id.layout_back);
        txt_action_title = (TextView)findViewById(R.id.txt_action_title);
        txt_action_title.setText("Edit Profile");

        spinner_state = (Spinner) findViewById(R.id.spinner_state);
    }

    private void getDetails()
    {

        Data user = new Data();

        user = getUserDetails();

        edit_first_name.setText(user.getFirstName());
        edit_last_name.setText(user.getLastName());
        edit_dob.setText(user.getDob());
        edit_email.setText(user.getEmail());
        edit_mobile.setText(user.getMobileNumber());
//        edit_state.setText(user.getState());
//        Arrays.asList(states).indexOf(user.getState())
//        Toast.makeText(ctx,"state  :  "+Arrays.asList(states).indexOf(user.getState()),Toast.LENGTH_LONG).show();
        spinner_state.setSelection(Arrays.asList(states).indexOf(user.getState()),true);
        edit_city.setText(user.getCity());

        edit_email.setEnabled(false);
        edit_dob.setEnabled(false);

//        Toast.makeText(ctx,"DOB  :  "+SettingsPreferences.getConsumer(this).getDob(),Toast.LENGTH_LONG).show();
    }

    private void setTypeFace()
    {
        txt_action_title.setTypeface(getBoldTypeFace());

        btn_update.setTypeface(getBoldTypeFace());

        input_first_name.setTypeface(getRegularTypeFace());
        input_last_name.setTypeface(getRegularTypeFace());
        input_email.setTypeface(getRegularTypeFace());
        input_mobile.setTypeface(getRegularTypeFace());
//        input_state.setTypeface(getRegularTypeFace());
        input_city.setTypeface(getRegularTypeFace());
        input_dob.setTypeface(getRegularTypeFace());

        edit_first_name.setTypeface(getRegularTypeFace());
        edit_last_name.setTypeface(getRegularTypeFace());
        edit_dob.setTypeface(getRegularTypeFace());
        edit_email.setTypeface(getRegularTypeFace());
        edit_mobile.setTypeface(getRegularTypeFace());
//        edit_state.setTypeface(getRegularTypeFace());
        edit_city.setTypeface(getRegularTypeFace());
    }

    private void setClickListener()
    {
        layout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (edit_first_name.getText().toString().length() == 0) {
                    edit_first_name.setError("Please enter first name");
//                    Toast.makeText(ctx,"Please enter first name",Toast.LENGTH_LONG).show();
                } else if (edit_last_name.getText().toString().length() == 0) {
                    edit_last_name.setError("Please enter last name");
//                    Toast.makeText(ctx,"Please enter last name",Toast.LENGTH_LONG).show();
                } else if (edit_dob.getText().toString().length() == 0) {
                    edit_dob.setError("Please enter Date of Birth");
//                    Toast.makeText(ctx,"Please enter Date of Birth",Toast.LENGTH_LONG).show();
                } else if (edit_mobile.getText().toString().length() == 0) {
                    edit_mobile.setError("Please enter mobile number");
//                    Toast.makeText(ctx,"Please enter mobile number",Toast.LENGTH_LONG).show();
                } else if (edit_mobile.getText().toString().length() < 10) {
                    edit_mobile.setError("Please enter valid mobile number");
//                    Toast.makeText(ctx,"Please enter mobile number",Toast.LENGTH_LONG).show();
                }
               /* else if (edit_state.getText().toString().length() == 0) {
                    edit_state.setError("Please define your state");
//                    Toast.makeText(ctx,"Please define your state",Toast.LENGTH_LONG).show();
                } */
                else if (edit_city.getText().toString().length() == 0) {
                    edit_city.setError("Please define your city");
//                    Toast.makeText(ctx,"Please define your city",Toast.LENGTH_LONG).show();
                } else {
                    if (isInternet) {
                        sendSubScripRequest();
                    } else {
                        Toast.makeText(ctx, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinnerValue = adapterView.getItemAtPosition(i).toString();
                selectedItem = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setSpinnerAdapter()
    {
        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, states);



        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, states) {

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent)
            {
                View v = null;
                v = super.getDropDownView(position, null, parent);
                // If this is the selected item position
                if (position == selectedItem) {
                    v.setBackgroundColor(Color.LTGRAY);
                }
                else {
                    // for other views
                    v.setBackgroundColor(Color.WHITE);

                }
                return v;
            }
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // attaching data adapter to spinner
        spinner_state.setAdapter(dataAdapter);
        spinner_state.setSelection(5,true);
    }

    private void sendSubScripRequest() {
        try {

            String id_user = SettingsPreferences.getConsumer(this).getIdUser();
            String password = getLoginPwd();
            String fName = edit_first_name.getText().toString();
            String lName = edit_last_name.getText().toString();
            String dob = edit_dob.getText().toString();
            String city = edit_city.getText().toString();
//            String state = edit_state.getText().toString();
            spinnerValue = spinner_state.getSelectedItem().toString();
            String email = edit_email.getText().toString();
            String mobile = edit_mobile.getText().toString();
            String paymentMode = "1";
            String IMEI_number = getIMEINumber();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendChangeProfileRequest(id_user, fName, lName, dob, email, mobile, password, password,
                    spinnerValue, city, paymentMode,IMEI_number, new Callback<User>() {
                        @Override
                        public void success(User model, Response response) {

                            showWaitIndicator(false);

                            if (response.getStatus() == 200) {
                                try {
                                    Log.e("REGISTER_RESPONSE", "" + new Gson().toJson(model));

                                    if (model.getStatus() == 1) {
                                        Data data = model.getData();
                                        updateUser(data);

                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();

//                                        startActivity(new Intent(ctx, PaymentOptionActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(ctx, model.getMessage(), Toast.LENGTH_LONG).show();
                                    }


                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showWaitIndicator(false);

                            Toast.makeText(ctx, "failure", Toast.LENGTH_LONG).show();

                            Log.e("ERROR", "" + error.getMessage());
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}